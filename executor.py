#!/usr/bin/python

############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import sys
import pickle
import logging

from logging import handlers
from mpi4py import MPI

from ridge_function import (CosineRidge, SpectrumRegularizedCosineRidge)
from gfc_actions import GFCRequest
from alc_actions import AlCRequest
from feature_construction import (PickledDataGreedyFeatureConstructor, DistributedGreedyFeatureConstructor,
                                  PickledDataAlaCarteFeatureConstructor)


logger_ids = ['RidgeRegression', 'ConfigureRequest', 'LoadDataAndConfigureRequest', 'GFCRequest', 'GFCConstructFeatureBlock',
              'GFCCommunicateFeatureBlock', 'GFCFitLinearModel', 'GFCFeatureSelection', 'GFCCommunicateGSGBias', 'GFCPrintStatistics',
              'GFCCheckConvergence', 'GFCMPICheckConvergence', 'GFCStoreModel', 'AlCRequest', 'AlCConstructFeatureBlock', 'AlCStoreModel',
              'GreedyFeatureConstructor', 'PickledDataGreedyFeatureConstructor', 'DistributedGreedyFeatureConstructor',
              'AlaCarteFeatureConstructor', 'PickledDataAlaCarteFeatureConstructor', 'Executor']

logger = logging.getLogger('Executor')


def configure_file_log_handler(fpath, level=logging.INFO, max_file_size=5000000, num_backups=10):
    handler = handlers.RotatingFileHandler(fpath, maxBytes=max_file_size, backupCount=num_backups)
    formatter = logging.Formatter('[%(asctime)s] --- [%(levelname)8s] --- [%(name)30s] --- %(message)s')
    handler.setFormatter(formatter)
    handler.setLevel(level)
    return handler


def configure_logger(logger_id, log_fpath, log_level=logging.INFO, max_file_size=5000000, num_backups=10):
    logger = logging.getLogger(logger_id)
    logger.setLevel(log_level)
    logger.addHandler(configure_file_log_handler(log_fpath, log_level, max_file_size, num_backups))


def start_loggers(log_fpath, log_level=logging.INFO):
    for logger_id in logger_ids:
        configure_logger(logger_id, log_fpath, log_level=log_level)


def load_fold_args(fpath):
    f = open(fpath, 'rb')
    train_args, test_args = pickle.load(f)
    f.close()
    return train_args, test_args


def log_action_choice(selected_alg, selected_fold):
    logger.info('selected method: ' + selected_alg)
    logger.info('selected fold number: ' + str(selected_fold))


if __name__ == '__main__':
    alg = sys.argv[1]
    fpath_prefix = sys.argv[2]
    outer_cv_fold_num = int(sys.argv[3])
    train_args, test_args = load_fold_args(fpath_prefix + '-fold-args-' + str(outer_cv_fold_num))
    model_path = sys.argv[4]

    feature_constructor, fc_request = None, None
    if alg == 'gfc' or alg == 'mpi-gfc':
        cosine_ridge = CosineRidge(init_reg_param=float(sys.argv[9]))
        # cosine_ridge = SpectrumRegularizedCosineRidge(init_reg_param=float(sys.argv[9]))
        fc_request = GFCRequest(gsg_ridge_f=cosine_ridge, max_lin_model_fits=int(sys.argv[5]), num_data_splits=int(sys.argv[6]),
                                gsg_max_descent_steps=int(sys.argv[7]), split_sz=int(sys.argv[8]), lm_init_reg_param=float(sys.argv[10]),
                                feat_cut_off_fraction=float(sys.argv[11]), tolerance=float(sys.argv[12]))
        if alg == 'gfc':
            start_loggers(model_path + '.log')
            log_action_choice(alg, outer_cv_fold_num)
            feature_constructor = PickledDataGreedyFeatureConstructor(data_path=fpath_prefix + '.dat', train_args=train_args,
                                                                      test_args=test_args, model_path=model_path)
        else:
            start_loggers(model_path + '.mpi-machine_' + str(MPI.COMM_WORLD.Get_rank()) + '.log')
            log_action_choice(alg, outer_cv_fold_num)
            feature_constructor = DistributedGreedyFeatureConstructor(data_path=fpath_prefix + '.dat', train_args=train_args,
                                                                      test_args=test_args, model_path=model_path)
    elif alg == 'alc':
        start_loggers(model_path + '.log')
        log_action_choice(alg, outer_cv_fold_num)
        fc_request = AlCRequest(num_feats_per_cmp=int(sys.argv[6]), num_components=int(sys.argv[5]), lm_init_reg_param=float(sys.argv[7]))
        feature_constructor = PickledDataAlaCarteFeatureConstructor(fpath_prefix + '.dat', train_args, test_args, model_path=model_path)

    feature_constructor.execute(fc_request)
