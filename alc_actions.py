############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import multiprocessing as mp
import ctypes as ct
import pickle
import time
import logging

from scipy.optimize import fmin_l_bfgs_b

from linear_regression import RidgeRegression
from fc_actions import (FCRequest, FCAction)


def mp_mse_pool_init(X, y):
    global mse_mp_X, mse_mp_y
    mse_mp_X, mse_mp_y = X, y


def mp_mse_gradient_pool_init(X, y, F, Z, U):
    global mse_grad_mp_X, mse_grad_mp_y, mse_grad_mp_F, mse_grad_mp_Z, mse_grad_mp_U
    mse_grad_mp_X, mse_grad_mp_y, mse_grad_mp_F, mse_grad_mp_Z, mse_grad_mp_U = X, y, F, Z, U


def alc_fold_mse(tr_fold_args, vl_fold_args, gamma):
    np_y = np.frombuffer(mse_mp_y).reshape(-1)
    np_X = np.frombuffer(mse_mp_X).reshape(np_y.shape[0], -1)
    amplitudes = RidgeRegression.fit(np_X[tr_fold_args, :], np_y[tr_fold_args], gamma)
    residues = np_X[vl_fold_args, :].dot(amplitudes).reshape(-1) - np_y[vl_fold_args].reshape(-1)
    return np.mean(residues ** 2)


def alc_fold_mse_gradient(tr_fold_args, vl_fold_args, nu, gamma):
    y = np.frombuffer(mse_grad_mp_y).reshape(-1)
    X = np.frombuffer(mse_grad_mp_X).reshape(y.shape[0], -1)
    n, q, dim = tr_fold_args.shape[0], nu.shape[0], X.shape[1]
    dmu, dsigma, dnu, dgamma = np.zeros((q, dim)), np.zeros((q, dim)), np.zeros(q), np.zeros(1)
    F, Z = np.frombuffer(mse_grad_mp_F).reshape(X.shape[0], -1), np.frombuffer(mse_grad_mp_Z).reshape(X.shape[0], -1)
    U = np.frombuffer(mse_grad_mp_U).reshape(-1, dim)

    amplitudes, K = RidgeRegression.fit_with_penalty_mat(F[tr_fold_args, :], y[tr_fold_args], gamma)
    residues = F[vl_fold_args, :].dot(amplitudes).reshape(-1) - y[vl_fold_args].reshape(-1)
    t = np.linalg.solve(K, np.mean(np.multiply(residues.reshape(-1, 1), F[vl_fold_args, :]), axis=0))
    del K  # release the memory consumed by this temporary matrix

    Fa, Ft = F[tr_fold_args, :].dot(amplitudes).reshape(-1), F[tr_fold_args, :].dot(t).reshape(-1)
    dgamma[0] -= 2 * t.dot(amplitudes) * gamma

    current_col, cos_block_sz, feat_block_sz = 0, U.shape[0], U.shape[0] / q
    for i in range(q):
        cos_start, cos_stop = current_col, current_col + feat_block_sz
        sin_start, sin_stop = cos_block_sz + cos_start, cos_block_sz + cos_stop

        F_sin_a_cos = F[:, sin_start:sin_stop].dot(amplitudes[cos_start:cos_stop]).reshape(-1)
        F_cos_a_sin = F[:, cos_start:cos_stop].dot(amplitudes[sin_start:sin_stop]).reshape(-1)
        train_F_sin_t_cos = F[tr_fold_args, :][:, sin_start:sin_stop].dot(t[cos_start:cos_stop]).reshape(-1)
        train_F_cos_t_sin = F[tr_fold_args, :][:, cos_start:cos_stop].dot(t[sin_start:sin_stop]).reshape(-1)

        vl_Fa_mixed = -F_sin_a_cos[vl_fold_args] + F_cos_a_sin[vl_fold_args]
        train_Ft_mixed = -train_F_sin_t_cos + train_F_cos_t_sin
        dmu[i, :] += np.mean(np.multiply(np.multiply(residues, vl_Fa_mixed).reshape(-1, 1), X[vl_fold_args, :]), axis=0).reshape(-1)
        dmu[i, :] += np.mean(np.multiply(np.multiply(y[tr_fold_args].reshape(-1), train_Ft_mixed).reshape(-1, 1), X[tr_fold_args, :]),
                             axis=0).reshape(-1)
        dmu[i, :] += np.mean(np.multiply(np.multiply(Fa, train_F_sin_t_cos).reshape(-1, 1), X[tr_fold_args, :]), axis=0).reshape(-1)
        dmu[i, :] += np.mean(np.multiply(np.multiply(Ft, F_sin_a_cos[tr_fold_args]).reshape(-1, 1), X[tr_fold_args, :]), axis=0).reshape(-1)
        dmu[i, :] -= np.mean(np.multiply(np.multiply(Fa, train_F_cos_t_sin).reshape(-1, 1), X[tr_fold_args, :]), axis=0).reshape(-1)
        dmu[i, :] -= np.mean(np.multiply(np.multiply(Ft, F_cos_a_sin[tr_fold_args]).reshape(-1, 1), X[tr_fold_args, :]), axis=0).reshape(-1)

        F_sin_U_cos_a_cos = F[:, sin_start:sin_stop].dot(np.multiply(U[cos_start:cos_stop, :],
                                                                     amplitudes[cos_start:cos_stop].reshape(-1, 1)))
        F_cos_U_cos_a_sin = F[:, cos_start:cos_stop].dot(np.multiply(U[cos_start:cos_stop, :],
                                                                     amplitudes[sin_start:sin_stop].reshape(-1, 1)))
        train_F_sin_U_cos_t_cos = F[tr_fold_args, :][:, sin_start:sin_stop].dot(np.multiply(U[cos_start:cos_stop, :],
                                                                                            t[cos_start:cos_stop].reshape(-1, 1)))
        train_F_cos_U_cos_t_sin = F[tr_fold_args, :][:, cos_start:cos_stop].dot(np.multiply(U[cos_start:cos_stop, :],
                                                                                            t[sin_start:sin_stop].reshape(-1, 1)))

        vl_F_U_a = -F_sin_U_cos_a_cos[vl_fold_args, :] + F_cos_U_cos_a_sin[vl_fold_args, :]
        train_F_U_a = -train_F_sin_U_cos_t_cos + train_F_cos_U_cos_t_sin
        dsigma[i, :] += np.mean(np.multiply(vl_F_U_a, np.multiply(X[vl_fold_args, :], residues.reshape(-1, 1))), axis=0).reshape(-1)
        dsigma[i, :] += np.mean(np.multiply(train_F_U_a, np.multiply(X[tr_fold_args, :], y[tr_fold_args].reshape(-1, 1))),
                                axis=0).reshape(-1)
        dsigma[i, :] += np.mean(np.multiply(np.multiply(Fa.reshape(-1, 1), train_F_sin_U_cos_t_cos), X[tr_fold_args, :]),
                                axis=0).reshape(-1)
        dsigma[i, :] += np.mean(np.multiply(np.multiply(Ft.reshape(-1, 1), F_sin_U_cos_a_cos[tr_fold_args, :]), X[tr_fold_args, :]),
                                axis=0).reshape(-1)
        dsigma[i, :] -= np.mean(np.multiply(np.multiply(Fa.reshape(-1, 1), train_F_cos_U_cos_t_sin), X[tr_fold_args, :]),
                                axis=0).reshape(-1)
        dsigma[i, :] -= np.mean(np.multiply(np.multiply(Ft.reshape(-1, 1), F_cos_U_cos_a_sin[tr_fold_args, :]), X[tr_fold_args, :]),
                                axis=0).reshape(-1)

        cos_Z_cos_a_cos = np.cos(Z[:, cos_start:cos_stop]).dot(amplitudes[cos_start:cos_stop]).reshape(-1)
        sin_Z_cos_a_sin = np.sin(Z[:, cos_start:cos_stop]).dot(amplitudes[sin_start:sin_stop]).reshape(-1)
        train_cos_Z_cos_t_cos = np.cos(Z[tr_fold_args, :][:, cos_start:cos_stop]).dot(t[cos_start:cos_stop]).reshape(-1)
        train_sin_Z_sin_t_sin = np.sin(Z[tr_fold_args, :][:, cos_start:cos_stop]).dot(t[sin_start:sin_stop]).reshape(-1)

        vl_Za_mixed = cos_Z_cos_a_cos[vl_fold_args] + sin_Z_cos_a_sin[vl_fold_args]
        train_Zt_matched = train_cos_Z_cos_t_cos + train_sin_Z_sin_t_sin
        dnu[i] += 2 * nu[i] * np.mean(np.multiply(vl_Za_mixed, residues))
        dnu[i] += 2 * nu[i] * np.mean(np.multiply(train_Zt_matched, y[tr_fold_args].reshape(-1)))
        dnu[i] -= 2 * nu[i] * np.mean(np.multiply(Fa, train_cos_Z_cos_t_cos))
        dnu[i] -= 2 * nu[i] * np.mean(np.multiply(Ft, cos_Z_cos_a_cos[tr_fold_args]))
        dnu[i] -= 2 * nu[i] * np.mean(np.multiply(Fa, train_sin_Z_sin_t_sin))
        dnu[i] -= 2 * nu[i] * np.mean(np.multiply(Ft, sin_Z_cos_a_sin[tr_fold_args]))

        current_col = cos_stop

    return 2 * np.append(np.append(np.append(dmu.reshape(-1), dsigma.reshape(-1)), dnu.reshape(-1)), dgamma)


class AlCRequest(FCRequest):

    def __init__(self, num_components=5, num_feats_per_cmp=100, num_inner_cv_folds=5, num_probations=10, max_probation_iters=20,
                 max_fmin_iters=200, lm_init_reg_param=1e-2):

        self.logger = logging.getLogger('AlCRequest')

        self.num_components = num_components
        self.num_feats_per_component = num_feats_per_cmp
        self.num_inner_cv_folds = num_inner_cv_folds
        self.lm_init_reg_param = lm_init_reg_param
        self.max_fmin_iters = max_fmin_iters
        self.num_probations = num_probations
        self.max_probation_iters = max_probation_iters
        self.num_features = self.num_components * self.num_feats_per_component
        self.max_mp_pool_threads = min(num_inner_cv_folds, mp.cpu_count())
        self.init_mean_scale = 1e-2
        self.cov_ignorance_threshold = 1e-6

        self.sm_U = None
        self.opt_mu = None
        self.opt_sigma = None
        self.opt_nu = None

        self.logger.debug('number of clusters: ' + str(self.num_components))
        self.logger.debug('number of features per cluster: ' + str(self.num_feats_per_component))
        self.logger.debug('number of inner cross-validation folds: ' + str(self.num_inner_cv_folds))
        self.logger.debug('initial value of the regularization parameter (linear ridge regression): ' + str(self.lm_init_reg_param))
        self.logger.debug('number of init-solution probations: ' + str(self.num_probations))
        self.logger.debug('max iterations per probation: ' + str(self.max_probation_iters))
        self.logger.debug('max fmin iterations (after best probation): ' + str(self.max_fmin_iters))

    def reset(self):
        super(AlCRequest, self).reset()

        self.sm_U = None
        self.opt_mu = None
        self.opt_sigma = None
        self.opt_nu = None

    def configure(self, X, y, train_args, test_args):
        self.sm_train_X = FCRequest.to_shared_memory(X[train_args, :])
        self.sm_train_y = FCRequest.to_shared_memory(y[train_args])
        self.sm_test_X = FCRequest.to_shared_memory(X[test_args, :])
        self.sm_test_y = FCRequest.to_shared_memory(y[test_args])
        self.test_y_variance = np.mean((y[test_args] - np.mean(y[test_args])) ** 2)

        self.train_sz = train_args.shape[0]
        self.test_sz = test_args.shape[0]
        self.dim = X.shape[1]

        self._set_shared_mem_standard_vectors()
        self.set_inner_cv_folds()

    def _set_shared_mem_standard_vectors(self):
        self.sm_U = mp.RawArray(ct.c_double, self.num_features * self.dim)
        U = np.frombuffer(self.sm_U).reshape(self.num_features, self.dim)
        U[:, :] = np.random.standard_normal(size=(self.num_features, self.dim))

    def initialize_hparams(self, as_vector=False, init_reg_param=None):
        X = np.frombuffer(self.sm_train_X).reshape(-1, self.dim)
        y = np.frombuffer(self.sm_train_y).reshape(-1)
        mu = np.random.standard_normal(size=(self.num_components, self.dim))
        mu = self.init_mean_scale * np.multiply(mu, 1. / np.linalg.norm(mu, axis=1).reshape(-1, 1), mu)
        x_range_vec = np.max(X, axis=0).reshape(1, -1) - np.min(X, axis=0).reshape(1, -1)
        sigma = np.multiply(x_range_vec * np.sqrt(self.dim), np.multiply(0.4 * (1 + np.random.rand(self.num_components, 1)),
                                                                         np.ones((self.num_components, self.dim))))
        sigma = np.where(sigma < self.cov_ignorance_threshold, sigma, 1. / sigma)
        nu = (np.std(y) / self.num_components) * np.ones(self.num_components)

        if init_reg_param is None:
            gamma = np.array([self.lm_init_reg_param])
        else:
            gamma = np.array([init_reg_param])

        if as_vector:
            return np.append(np.append(np.append(mu.reshape(-1), sigma.reshape(-1)), nu.reshape(-1)), gamma)

        return mu, sigma, nu, gamma

    def unzip_hparams(self, hparams):
        block_sz = self.num_components * self.dim
        mu = hparams[:block_sz].reshape(self.num_components, self.dim)
        sigma = hparams[block_sz: 2 * block_sz].reshape(self.num_components, self.dim)
        nu = hparams[2 * block_sz: -1].reshape(-1)
        gamma = hparams[-1]
        return mu, sigma, nu, gamma

    def __map_X_to_sm_F(self, X):
        U = np.frombuffer(self.sm_U).reshape(-1, self.dim)
        sm_F = mp.RawArray(ct.c_double, 2 * X.shape[0] * self.num_features)
        F = np.frombuffer(sm_F).reshape(X.shape[0], -1)
        AlaCarte.transform(X, U, self.opt_mu, self.opt_sigma, self.opt_nu, F)
        return sm_F

    def map_data_to_alc_features(self):
        self.sm_train_F = self.__map_X_to_sm_F(np.frombuffer(self.sm_train_X).reshape(-1, self.dim))
        self.sm_test_F = self.__map_X_to_sm_F(np.frombuffer(self.sm_test_X).reshape(-1, self.dim))


class AlaCarte(object):

    @staticmethod
    def transform(X, U, mu, sigma, nu, F=None, Z=None):
        num_components = sigma.shape[0]
        num_feats_per_component = U.shape[0] / num_components
        if F is None:  # this is not an in-place transformation
            F = np.zeros((X.shape[0], 2 * U.shape[0]))

        if Z is None:
            Z = np.zeros((X.shape[0], U.shape[0]))

        col_cursor = 0
        for i in range(num_components):
            col_start, col_end = col_cursor, col_cursor + num_feats_per_component
            Z[:, col_start:col_end] = X.dot(np.multiply(U[col_start:col_end, :], sigma[i, :].reshape(1, -1)).T)
            Z[:, col_start:col_end] += X.dot(mu[i, :]).reshape(-1, 1)
            col_cursor = col_end
        F[:, :U.shape[0]] = np.cos(Z, F[:, :U.shape[0]])  # block I is for cos features
        F[:, U.shape[0]:] = np.sin(Z, F[:, U.shape[0]:])  # block II is for sin features
        t = np.multiply(np.ones((num_components, num_feats_per_component)), np.square(nu).reshape(-1, 1)).reshape(-1)
        np.multiply(F, np.append(t, t).reshape(1, -1), F)

        return F, Z

    @staticmethod
    def mse(hparams, alc_request):
        U = np.frombuffer(alc_request.sm_U).reshape(-1, alc_request.dim)
        mu, sigma, nu, gamma = alc_request.unzip_hparams(hparams)
        sm_train_F = mp.RawArray(ct.c_double, 2 * alc_request.train_sz * alc_request.num_features)
        F = np.frombuffer(sm_train_F).reshape(alc_request.train_sz, -1)
        AlaCarte.transform(np.frombuffer(alc_request.sm_train_X).reshape(-1, alc_request.dim), U, mu, sigma, nu, F)

        async_results = []
        processor_pool = mp.Pool(alc_request.max_mp_pool_threads, mp_mse_pool_init, (sm_train_F, alc_request.sm_train_y))
        for tr_fold_args, vl_fold_args in alc_request.inner_cv_args:
            async_results.append(processor_pool.apply_async(alc_fold_mse, (tr_fold_args, vl_fold_args, gamma)))
        processor_pool.close()
        processor_pool.join()

        val = 0.
        for i in range(alc_request.num_inner_cv_folds):
            val += async_results[i].get()
        del sm_train_F, F, async_results

        return val / alc_request.num_inner_cv_folds

    @staticmethod
    def mse_gradient(hparams, alc_request):
        U = np.frombuffer(alc_request.sm_U).reshape(-1, alc_request.dim)
        mu, sigma, nu, gamma = alc_request.unzip_hparams(hparams)
        sm_train_F = mp.RawArray(ct.c_double, 2 * alc_request.train_sz * alc_request.num_features)
        sm_train_Z = mp.RawArray(ct.c_double, alc_request.train_sz * alc_request.num_features)
        F, Z = np.frombuffer(sm_train_F).reshape(alc_request.train_sz, -1), np.frombuffer(sm_train_Z).reshape(alc_request.train_sz, -1)
        AlaCarte.transform(np.frombuffer(alc_request.sm_train_X).reshape(-1, alc_request.dim), U, mu, sigma, nu, F, Z)

        async_results = []
        processor_pool = mp.Pool(alc_request.max_mp_pool_threads, mp_mse_gradient_pool_init, (alc_request.sm_train_X,
                                                                                              alc_request.sm_train_y, sm_train_F,
                                                                                              sm_train_Z, alc_request.sm_U))
        for tr_fold_args, vl_fold_args in alc_request.inner_cv_args:
            async_results.append(processor_pool.apply_async(alc_fold_mse_gradient, (tr_fold_args, vl_fold_args, nu, gamma)))
        processor_pool.close()
        processor_pool.join()

        gradient = np.zeros(hparams.shape[0])
        for i in range(alc_request.num_inner_cv_folds):
            gradient += async_results[i].get()
        del sm_train_F, sm_train_Z, F, Z, async_results

        return gradient / alc_request.num_inner_cv_folds


class AlCConstructFeatureBlock(FCAction):

    def __init__(self, next_action=None, init_reg_param_log_space_sz=5, init_reg_param_log_space_lb=-2, init_reg_param_log_space_ub=1):
        self.next_action = next_action
        self.logger = logging.getLogger('AlCConstructFeatureBlock')

        self.__reg_params_sz = init_reg_param_log_space_sz
        self.__init_reg_params = np.logspace(init_reg_param_log_space_lb, init_reg_param_log_space_ub, init_reg_param_log_space_sz)

    def __log_fmin_probation(self, fmin_output, prob_num=None):
        if prob_num is not None:
            self.logger.info('probation ' + str(prob_num))
        else:
            self.logger.info('fmin from the best probation stop:')
        self.logger.info('--> fmin status: ' + fmin_output[2]['task'])
        self.logger.info('--> fmin objective val: ' + str(fmin_output[1]))

    def _process_request(self, alc_request):
        hp_start_t = time.time()
        opt_hparams, opt_val = None, 1e+10
        for i in range(alc_request.num_probations):
            prob_start_t = time.time()
            hparams = alc_request.initialize_hparams(as_vector=True, init_reg_param=self.__init_reg_params[i % self.__reg_params_sz])
            fmin_output = fmin_l_bfgs_b(lambda z: AlaCarte.mse(z, alc_request), hparams, lambda z: AlaCarte.mse_gradient(z, alc_request),
                                        disp=False, iprint=-1, maxiter=alc_request.max_probation_iters)
            if fmin_output[1] < opt_val:
                opt_hparams, opt_val = fmin_output[0], fmin_output[1]
            self.__log_fmin_probation(fmin_output, i + 1)
            self.logger.info('[probation] TIME: ' + str(time.time() - prob_start_t))
        opt_output = fmin_l_bfgs_b(lambda z: AlaCarte.mse(z, alc_request), opt_hparams, lambda z: AlaCarte.mse_gradient(z, alc_request),
                                   disp=False, iprint=-1, maxiter=alc_request.max_fmin_iters)
        self.__log_fmin_probation(opt_output)
        self.logger.info('[fmin] TIME: ' + str(time.time() - hp_start_t))

        alc_request.opt_mu, alc_request.opt_sigma, alc_request.opt_nu, gamma = alc_request.unzip_hparams(opt_output[0])
        alc_request.map_data_to_alc_features()

        lm_start_t = time.time()
        train_F = np.frombuffer(alc_request.sm_train_F).reshape(alc_request.train_sz, -1)
        test_F = np.frombuffer(alc_request.sm_test_F).reshape(alc_request.test_sz, -1)
        train_y, test_y = np.frombuffer(alc_request.sm_train_y).reshape(-1), np.frombuffer(alc_request.sm_test_y).reshape(-1)

        self.logger.info('--> optimal lambda: ' + str(abs(gamma)))
        alc_request.amplitudes = RidgeRegression.fit(train_F, train_y, gamma)
        self.logger.info('[linear model] TIME: ' + str(time.time() - lm_start_t))

        alc_request.log_residue_stats(train_F.dot(alc_request.amplitudes) - train_y)
        test_residuals = test_F.dot(alc_request.amplitudes) - test_y
        alc_request.log_residue_stats(test_residuals, prefix='test')
        alc_request.log_r2(test_residuals)


class AlCStoreModel(FCAction):

    def __init__(self, model_path, train_args, test_args):
        self.next_action = None
        self.logger = logging.getLogger('AlCStoreModel')

        self.model_path = model_path
        self.train_args = train_args
        self.test_args = test_args

    def _process_request(self, alc_request):
        f = open(self.model_path, 'wb')
        pickle.dump((np.frombuffer(alc_request.sm_U).reshape(-1, alc_request.dim), alc_request.opt_mu, alc_request.opt_sigma,
                     alc_request.opt_nu, alc_request.amplitudes, self.train_args, self.test_args), f)
        f.close()
