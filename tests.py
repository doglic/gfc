############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import multiprocessing as mp
import ctypes as ct
import unittest
import logging

from linear_regression import (RidgeRegression, UnsupervisedKFold)
from ridge_function import (CosineRidge, SpectrumRegularizedCosineRidge)
from alc_actions import (AlaCarte, AlCRequest)


logger_ids = ['RidgeRegression', 'GFCRequest', 'AlCRequest', 'UnitTests']


class GFCUnitTests(unittest.TestCase):

    def setUp(self):
        self.logger = logging.getLogger('UnitTests')
        GFCUnitTests.__start_loggers()

        self.m, self.dim, self.num_inner_cv_folds, k = 1000, 10, 5, 5
        self.grad_test_precision = 1e-4  # the error is less than 0.01% of the gradient component value
        self.epsilon = 1e-6  # perturbation for the estimation of partial derivatives

        self.X, U = np.random.rand(self.m, self.dim), np.random.standard_normal(size=(self.dim, k))
        scaling_vec = 1. / np.linalg.norm(U, axis=0).reshape(1, -1)
        U = np.multiply(U, scaling_vec)
        self.y = np.sum(np.tan(self.X.dot(U)), axis=1).reshape(-1)
        self.y = (self.y - np.mean(self.y)) / (np.max(self.y) - np.min(self.y))
        self.bias = np.ones(self.m) * np.mean(self.y)

    @staticmethod
    def __configure_logger(logger_id, log_level=logging.DEBUG):
        logger = logging.getLogger(logger_id)
        logger.setLevel(log_level)
        logger.addHandler(logging.StreamHandler())

    @staticmethod
    def __start_loggers(log_level=logging.DEBUG):
        for logger_id in logger_ids:
            GFCUnitTests.__configure_logger(logger_id, log_level=log_level)

    def __ridge_function_gradient_test(self, ridge_f):
        cv_args = UnsupervisedKFold.kfolds(self.m, self.num_inner_cv_folds)
        x0 = ridge_f.initialize_hparams(self.dim)
        shift_mat = np.identity(x0.shape[0])
        gradient = ridge_f.mse_gradient(x0, self.X, self.y, cv_args, self.bias)
        for i in range(x0.shape[0]):
            shift = self.epsilon * shift_mat[i, :]
            left_shift_val = ridge_f.mse(x0 - shift, self.X, self.y, cv_args, self.bias)
            right_shift_val = ridge_f.mse(x0 + shift, self.X, self.y, cv_args, self.bias)
            est_grad = (right_shift_val - left_shift_val) / (2 * self.epsilon)
            self.logger.info('component: ' + str(i + 1))
            self.logger.info('estimated grad = ' + str(est_grad) + '; actual grad = ' + str(gradient[i]))

            relative_err = abs((est_grad - gradient[i]) / max(abs(gradient[i]), abs(est_grad)))
            self.logger.info('relative error = ' + str(relative_err))

            if abs(est_grad) < 1e-8 and abs(gradient[i]) < 1e-8 and est_grad * gradient[i] >= 0:
                self.logger.warn('skipping the precision check because the gradient and its estimate are in abs-val less than 1e-8...')
                continue
            self.assertTrue(relative_err < self.grad_test_precision)

    def test_cosine_ridge_gradient(self):
        gamma_candidates = np.logspace(-2, 1, 10)
        for gamma in gamma_candidates:
            self.logger.info('lambda = ' + str(gamma))
            self.__ridge_function_gradient_test(CosineRidge(init_reg_param=gamma))
            self.logger.info('\n')

    def test_spectrum_regularized_cosine_ridge_gradient(self):
        gamma_candidates = np.logspace(-2, 1, 10)
        for gamma in gamma_candidates:
            self.logger.info('lambda = ' + str(gamma))
            self.__ridge_function_gradient_test(SpectrumRegularizedCosineRidge(init_reg_param=gamma))
            self.logger.info('\n')

    def test_ridge_regression_gradient(self):
        mp_X, mp_y = mp.RawArray(ct.c_double, self.m * self.dim), mp.RawArray(ct.c_double, self.m)
        np_X, np_y = np.frombuffer(mp_X).reshape(-1, self.dim), np.frombuffer(mp_y)
        np_X[: , :], np_y[:] = self.X, self.y
        cv_args = UnsupervisedKFold.kfolds(self.m, self.num_inner_cv_folds)
        gamma_candidates = np.logspace(-2, 1, 10)
        for gamma in gamma_candidates:
            self.logger.info('lambda = ' + str(gamma))
            shift = self.epsilon * np.asarray([1.])
            left_shift_val = RidgeRegression.mse(gamma - shift, mp_X, mp_y, cv_args)
            right_shift_val = RidgeRegression.mse(gamma + shift, mp_X, mp_y, cv_args)
            est_grad = (right_shift_val - left_shift_val) / (2 * self.epsilon)
            gradient = RidgeRegression.mse_gradient(gamma, mp_X, mp_y, cv_args)
            self.logger.info('estimated grad = ' + str(est_grad) + '; actual grad = ' + str(gradient))

            relative_err = abs((est_grad - gradient) / max(abs(gradient), abs(est_grad)))
            self.logger.info('relative error = ' + str(relative_err) + '\n')
            self.assertTrue(relative_err < self.grad_test_precision)

    def test_a_la_carte_gradient(self):
        gamma_candidates = np.logspace(-2, 1, 5)
        for gamma in gamma_candidates:
            self.logger.info('lambda = ' + str(gamma))
            alc_request = AlCRequest(lm_init_reg_param=gamma)
            alc_request.configure(self.X, self.y, np.arange(self.m, dtype=int), np.arange(self.m, dtype=int))
            h0 = alc_request.initialize_hparams(as_vector=True)
            shift_mat = np.identity(h0.shape[0])
            gradient = AlaCarte.mse_gradient(h0, alc_request)
            for i in range(h0.shape[0]):
                shift = self.epsilon * shift_mat[i, :]
                left_shift_val = AlaCarte.mse(h0 - shift, alc_request)
                right_shift_val = AlaCarte.mse(h0 + shift, alc_request)
                est_grad = (right_shift_val - left_shift_val) / (2 * self.epsilon)
                self.logger.info('component: ' + str(i + 1))
                self.logger.info('estimated grad = ' + str(est_grad) + '; actual grad = ' + str(gradient[i]))

                relative_err = abs((est_grad - gradient[i]) / max(abs(gradient[i]), abs(est_grad)))
                self.logger.info('relative error = ' + str(relative_err))
                if abs(est_grad) < 1e-7 and abs(gradient[i]) < 1e-7 and est_grad * gradient[i] >= 0:
                    self.logger.warn('skipping the precision check because the gradient and its estimate are in abs-val less than 1e-8...')
                    continue
                self.assertTrue(relative_err < self.grad_test_precision)
            self.logger.info('\n')

if __name__ == '__main__':
    unittest.main()
