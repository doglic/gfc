############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np

from abc import (ABCMeta, abstractmethod)

from linear_regression import RidgeRegression


class RidgeFunction(object):

    __metaclass__ = ABCMeta

    '''
        Default value of the regularization parameter used in training of linear ridge regression model
    '''
    init_reg_param = 1e-2

    def _fold_mse(self, hparams, F, y, tr_fold_args, vl_fold_args):
        amplitudes = RidgeRegression.fit(F[tr_fold_args, :], y[tr_fold_args], RidgeFunction.regularization_param(hparams))
        residues = F[vl_fold_args, :].dot(amplitudes) - y[vl_fold_args]
        return np.mean(residues ** 2)

    '''
        The method computes the average mean squared error over validation samples in a k-fold cross-validation of linear ridge regression
        model specified with a regularization parameter and trained in a feature space defined by a bias and a ridge function-feature(s)

        A ridge function-feature is specified with a spectrum vector
        (e.g., feature(x) = vec(sin(<spectrum, x>), cos(<spectrum, x>)), where < , > denotes the dot product and vec is a vector with comma
        separated components)

        Parameters:

            hparams : numpy array
                      the array consists of a ridge spectrum and a regularization parameter (concatenated in this order)
                      shape=(d + 1,), where d is the dimension of the problem

            X : numpy array
                data matrix with instances as rows
                shape=(m, d), where m is the number of instances and d is the dimension of the problem

            y : numpy array
                the array with a target value per instance
                shape=(m,), where m is the number of instances

            cv_args : sklearn.KFold
                      indices of a k-fold data splitting

            bias : [Optional] numpy array
                   the feature space of linear model consists of bias and ridge feature(s) and this parameter specifies the bias
                   shape=(m,), where m is the number of instances

        Outputs:

            mse : float
                  the average mean squared error over validation samples in a k-fold cross-validation of linear ridge regression model
                  specified with a hyperparameter vector (@parameters: hparams)
    '''
    def mse(self, hparams, X, y, cv_args, bias=None):
        F = self.transform(X, RidgeFunction.spectrum(hparams), bias)[0]

        val = 0.
        for vl_fold_args, tr_fold_args in cv_args:
            val += self._fold_mse(hparams, F, y, tr_fold_args, vl_fold_args)

        return val / len(cv_args)

    @abstractmethod
    def _fold_mse_gradient(self, hparams, X, y, F, z, tr_fold_args, vl_fold_args):
        pass

    '''
        The method computes the gradient of the average mean squared error computed over validation samples in a k-fold cross-validation of
        linear regression model specified with a regularization parameter and trained in a feature space defined by bias and ridge features

        Parameters:

            hparams : numpy array
                      the array consists of a ridge spectrum and a regularization parameter (concatenated in this order)
                      shape=(d + 1,), where d is the dimension of the problem

            X : numpy array
                data matrix with instances as rows
                shape=(m, d), where m is the number of instances and d is the dimension of the problem

            y : numpy array
                the array with a target value per instance
                shape=(m,), where m is the number of instances

            cv_args : sklearn.KFold
                      indices of a k-fold data splitting

            bias : [Optional] numpy array
                   the feature space of linear model consists of bias and ridge feature(s) and this parameter specifies the bias
                   shape=(m,), where m is the number of instances

        Outputs:

            mse_gradient : numpy array
                           the gradient at the provided hyperparameter vector (@parameters: hparams) of the average mean squared error
                           objective function (@method: RidgeFunction.mse)
                           shape=(d + 1,), where d is the dimension of the problem
    '''
    def mse_gradient(self, hparams, X, y, cv_args, bias=None):
        F, z = self.transform(X, RidgeFunction.spectrum(hparams), bias)

        gradient = np.zeros(hparams.shape[0])
        for vl_fold_args, tr_fold_args in cv_args:
            gradient += self._fold_mse_gradient(hparams, X, y, F, z, tr_fold_args, vl_fold_args)

        return gradient / len(cv_args)

    '''
        Transform the data matrix to a new matrix having a bias feature at the first column (if provided) and ridge features at the
        remaining columns

        Parameters:

            X : numpy array
                data matrix with instances as rows
                shape=(m, d), where m is the number of instances and d is the dimension of the problem

            spectra : numpy array
                      matrix with ridge spectrum vectors as columns
                      shape=(d, k), where d is the dimension of the problem and k is the size of spectra

            bias : [Optional] numpy array
                   vector corresponding to a bias feature
                   shape=(m,), where m is the number of instances

        Outputs:

            F : numpy array
                new feature representation
                shape=(m, b + r), where m is the number of instances, b is one when bias is given, and r is the number of ridge components
                (note that r is a multiple of k, @parameters: spectra)

            Z : numpy array
                projection of the data matrix over the spectra
                shape=(m, k), where m is the number of instances and k is the size of spectra
    '''
    @abstractmethod
    def transform(self, X, spectra, bias=None):
        pass

    '''
        The method samples a random hyperparameter vector
        (ridge spectrum is set to a standard normal vector with components divided with a square root of the dimension of the problem)

        Parameters:

            spectrum : int
                       dimension of the problem/spectrum vector

            lm_init_reg_param : [Optional] float
                                initial value of the regularization parameter to be used in the linear ridge regression model fitting

        Outputs:

            init_sol : numpy array
                       hyperparameter vector that consists of a spectrum and a regularization parameter (concatenated in this order)
                       shape=(d + 1,), where d is the dimension of the problem
    '''
    def initialize_hparams(self, spectrum_dim, lm_init_reg_param=None):
        spectrum = np.random.standard_normal(size=spectrum_dim) / np.sqrt(spectrum_dim)
        if lm_init_reg_param is None:
            return np.append(spectrum, self.init_reg_param)
        return np.append(spectrum, lm_init_reg_param)

    '''
        The method reserves a memory block for a new data representation

        Parameters:

            num_instances : int
                            number of instances

            spectra_sz : int
                         size of spectra

            with_bias : bool
                        indicator for the (non)existence of a bias feature

        Outputs:

            M : numpy array
                memory block for a new data representation
                shape=(m, r + b), where m is the number of instances, b is one when there is a bias, and r is the number of ridge components
    '''
    def initialize_representation(self, num_instances, spectra_sz, with_bias=True):
        return np.zeros((num_instances, self.representation_dim(spectra_sz, with_bias)))

    '''
        The method computes the number of ridge components in a new data representation

        Parameters:

            spectra_sz : int
                         size of spectra

            with_bias : bool
                        indicator for the (non)existence of a bias feature

        Outputs:

            representation_dim : int
                                 the number of ridge components in a new data representation
    '''
    def representation_dim(self, spectra_sz, with_bias=False):
        if with_bias:
            return spectra_sz + 1
        return spectra_sz

    '''
        The method computes the size of the ridge spectra from the dimension of a ridge feature space representation

        Parameters:

            representation_dim : int
                                 number of features (ridge components and bias if provided) in a ridge feature space representation

            with_bias : bool
                        indicator for the (non)existence of a bias feature

        Outputs:

            spectrum_dim : int
                           size of the ridge spectra
    '''
    def spectra_dim(self, representation_dim, with_bias=True):
        if with_bias:
            return representation_dim - 1
        return representation_dim

    '''
        The method extracts the spectrum from a hyperparameter vector

        Parameters:

            hparams : numpy array
                      hyperparameter vector
                      shape=(d + 1,), where d is the dimension of the problem

        Outputs:

            spectrum : numpy array
                       spectrum vector that defines a ridge feature
                       shape=(d,), where d is the dimension of the problem
    '''
    @staticmethod
    def spectrum(hparams):
        return hparams[:-1]

    '''
        The method extracts the regularization parameter from a hyperparameter vector

        Parameters:

            hparams : numpy array
                      hyperparameter vector
                      shape=(d + 1,), where d is the dimension of the problem

        Outputs:

            regularization_param : float
                                   regularization parameter to be used in the fitting of linear ridge regression model
    '''
    @staticmethod
    def regularization_param(hparams):
        return hparams[-1]

    '''
        The method finds a set of amplitude indices that correspond to a provided set of spectra indices

        Parameters:

            spectra_indices : numpy array
                              set of spectra indices
                              shape=(s,)

        Outputs:

            amplitude_indices : numpy array
                                set of amplitude indices
                                shape=(t,), where t is not necessarily equal to s
    '''
    @abstractmethod
    def amplitude_indices(self, spectra_indices):
        pass

    '''
        The method finds a set of spectra indices corresponding to a set of linear model coefficients (i.e., amplitudes) that have larger
        absolute values than a specified cut-off

        Parameters:

            amplitudes : numpy array
                         linear model coefficients
                         shape=(t,)

            cut_off : float
                      retain only amplitudes with absolute values larger than this cut-off

        Outputs:

            active_spectra_indices : numpy array
                                     set of spectra indices satisfying the cut-off condition
                                     shape=(s,)
    '''
    @abstractmethod
    def active_spectra_indices(self, amplitudes, cut_off):
        pass


'''
    CosineRidge

        x |--> vec(bias, sin(<spectrum, x>), cos(<spectrum, x>))
        (from one spectrum vector two ridge features are derived)
'''


class CosineRidge(RidgeFunction):

    def __init__(self, init_reg_param=1e-2):
        self.init_reg_param = init_reg_param

    def representation_dim(self, spectra_sz, with_bias=False):
        if with_bias:
            return 2 * spectra_sz + 1
        return 2 * spectra_sz

    def spectra_dim(self, representation_dim, with_bias=True):
        if with_bias:
            return (representation_dim - 1) / 2
        return representation_dim / 2

    def active_spectra_indices(self, amplitudes, cut_off):
        sin_cols = np.arange(0, amplitudes.shape[0], 2)
        cos_cols = 1 + sin_cols
        active_spectra_indices = []
        for i in range(sin_cols.shape[0]):
            if abs(amplitudes[sin_cols[i]]) > cut_off and abs(amplitudes[cos_cols[i]]) > cut_off:
                active_spectra_indices.append(i)
        if len(active_spectra_indices) != 0:
            return np.asarray(active_spectra_indices)
        # all amplitudes are below cut-off threshold and there is no point in doing feature selection with this cut-off
        return np.arange(sin_cols.shape[0])

    def amplitude_indices(self, spectra_indices):
        if spectra_indices.shape[0] == 0:
            return spectra_indices
        active_cols = np.zeros(2 * (np.max(spectra_indices) + 1), dtype=bool)
        active_sin_indices = 2 * spectra_indices
        active_cos_indices = 1 + active_sin_indices
        active_cols[active_sin_indices] = active_cols[active_cos_indices] = True
        return np.argwhere(active_cols).reshape(-1)

    def transform(self, X, spectra, bias=None):
        if len(spectra.shape) == 1:
            spectra = spectra.reshape(-1, 1)

        Z = X.dot(spectra)
        n, m = Z.shape
        start_column = 0
        F = np.zeros((n, self.representation_dim(m, bias is not None)))
        if bias is not None:
            F[:, 0] = bias
            start_column += 1

        sin_cols = np.arange(start_column, F.shape[1], 2, dtype=int)
        cos_cols = sin_cols + 1
        F[:, sin_cols] = np.sin(Z, F[:, sin_cols])
        F[:, cos_cols] = np.cos(Z, F[:, cos_cols])

        return F, Z

    def _fold_mse_gradient(self, hparams, X, y, F, z, tr_fold_args, vl_fold_args):
        gradient = np.zeros(hparams.shape[0])
        sin_2z_tr, cos_2z_tr = np.sin(2 * z[tr_fold_args]), np.cos(2 * z[tr_fold_args])

        alpha, K = RidgeRegression.fit_with_penalty_mat(F[tr_fold_args, :], y[tr_fold_args], RidgeFunction.regularization_param(hparams))
        residue = F[vl_fold_args, :].dot(alpha) - y[vl_fold_args]
        t = np.linalg.solve(K, np.mean(np.multiply(residue.reshape(-1, 1), F[vl_fold_args, :]), axis=0))

        gradient[-1] -= 2 * RidgeFunction.regularization_param(hparams) * t.dot(alpha)
        gradient[:-1] += np.mean(np.multiply(np.multiply(alpha[1] * F[vl_fold_args, 2] - alpha[2] * F[vl_fold_args, 1],
                                                         residue).reshape(-1, 1), X[vl_fold_args, :]), axis=0)
        v = np.multiply(y[tr_fold_args], t[1] * F[tr_fold_args, 2] - t[2] * F[tr_fold_args, 1]).reshape(-1, 1)
        v -= (t[0] * alpha[1] + t[1] * alpha[0]) * np.multiply(F[tr_fold_args, 0], F[tr_fold_args, 2]).reshape(-1, 1)
        v += (t[0] * alpha[2] + t[2] * alpha[0]) * np.multiply(F[tr_fold_args, 0], F[tr_fold_args, 1]).reshape(-1, 1)
        v -= (t[1] * alpha[2] + t[2] * alpha[1]) * cos_2z_tr.reshape(-1, 1)
        v -= (t[1] * alpha[1] - t[2] * alpha[2]) * sin_2z_tr.reshape(-1, 1)
        gradient[:-1] += np.mean(np.multiply(v, X[tr_fold_args, :]), axis=0)

        return 2 * gradient


class SpectrumRegularizedCosineRidge(CosineRidge):

    def __init__(self, init_reg_param=1e-2):
        super(SpectrumRegularizedCosineRidge, self).__init__(init_reg_param)

    def _fold_mse(self, hparams, F, y, tr_fold_args, vl_fold_args):
        penalty_mat = np.zeros((3, 3))
        penalty_mat[0, 0] = np.mean(F[tr_fold_args, 0] ** 2)
        penalty_mat[1, 0] = penalty_mat[0, 1] = np.mean(np.multiply(F[tr_fold_args, 1], F[tr_fold_args, 0]))
        penalty_mat[2, 0] = penalty_mat[0, 2] = np.mean(np.multiply(F[tr_fold_args, 2], F[tr_fold_args, 0]))
        penalty_mat[2, 1] = penalty_mat[1, 2] = 0.5 * np.mean(2 * np.multiply(F[tr_fold_args, 1], F[tr_fold_args, 2]))
        cos_2z_tr_mean = np.mean(F[tr_fold_args, 2] ** 2 - F[tr_fold_args, 1] ** 2)
        penalty_mat[1, 1], penalty_mat[2, 2] = 0.5 * (1 - cos_2z_tr_mean), 0.5 * (1 + cos_2z_tr_mean)

        reg_param = RidgeFunction.regularization_param(hparams)
        amplitudes = RidgeRegression.fit_with_penalty_mat(F[tr_fold_args, :], y[tr_fold_args], reg_param, penalty_mat)[0]
        residues = F[vl_fold_args, :].dot(amplitudes) - y[vl_fold_args]
        return np.mean(residues ** 2)

    def _fold_mse_gradient(self, hparams, X, y, F, z, tr_fold_args, vl_fold_args):
        gradient = np.zeros(hparams.shape[0])
        sin_2z_tr, cos_2z_tr = np.sin(2 * z[tr_fold_args]), np.cos(2 * z[tr_fold_args])

        penalty_mat = np.zeros((3, 3))
        penalty_mat[0, 0] = np.mean(F[tr_fold_args, 0] ** 2)
        penalty_mat[1, 0] = penalty_mat[0, 1] = np.mean(np.multiply(F[tr_fold_args, 1], F[tr_fold_args, 0]))
        penalty_mat[2, 0] = penalty_mat[0, 2] = np.mean(np.multiply(F[tr_fold_args, 2], F[tr_fold_args, 0]))
        penalty_mat[2, 1] = penalty_mat[1, 2] = 0.5 * np.mean(sin_2z_tr)
        penalty_mat[1, 1], penalty_mat[2, 2] = 0.5 * (1 - np.mean(cos_2z_tr)), 0.5 * (1 + np.mean(cos_2z_tr))

        reg_param = RidgeFunction.regularization_param(hparams)
        alpha, K = RidgeRegression.fit_with_penalty_mat(F[tr_fold_args, :], y[tr_fold_args], reg_param, penalty_mat)
        residue = F[vl_fold_args, :].dot(alpha) - y[vl_fold_args]
        t = np.linalg.solve(K, np.mean(np.multiply(residue.reshape(-1, 1), F[vl_fold_args, :]), axis=0))

        sq_gamma = reg_param ** 2
        gradient[-1] -= 2 * reg_param * t.dot(penalty_mat.dot(alpha).reshape(-1))
        gradient[:-1] += np.mean(np.multiply(np.multiply(alpha[1] * F[vl_fold_args, 2] - alpha[2] * F[vl_fold_args, 1],
                                                         residue).reshape(-1, 1), X[vl_fold_args, :]), axis=0)
        v = np.multiply(y[tr_fold_args], t[1] * F[tr_fold_args, 2] - t[2] * F[tr_fold_args, 1]).reshape(-1, 1)
        v -= (1 + sq_gamma) * (t[0] * alpha[1] + t[1] * alpha[0]) * np.multiply(F[tr_fold_args, 0], F[tr_fold_args, 2]).reshape(-1, 1)
        v += (1 + sq_gamma) * (t[0] * alpha[2] + t[2] * alpha[0]) * np.multiply(F[tr_fold_args, 0], F[tr_fold_args, 1]).reshape(-1, 1)
        v -= (1 + sq_gamma) * (t[1] * alpha[2] + t[2] * alpha[1]) * cos_2z_tr.reshape(-1, 1)
        v -= (1 + sq_gamma) * (t[1] * alpha[1] - t[2] * alpha[2]) * sin_2z_tr.reshape(-1, 1)
        gradient[:-1] += np.mean(np.multiply(v, X[tr_fold_args, :]), axis=0)

        return 2 * gradient
