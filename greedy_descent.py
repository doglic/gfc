############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import time
import os

from scipy.optimize import fmin_l_bfgs_b

from ridge_function import CosineRidge
from linear_regression import (RidgeRegression, UnsupervisedKFold)


class GreedyDescent(object):

    def __init__(self, th_id=0, max_descent_steps=20, num_cv_folds=5, num_probations=10, max_probation_iters=20, max_fmin_iters=200,
                 ridge_f=CosineRidge(), tolerance=1e-2, max_succ_steps_no_improvement=3, init_reg_param_log_space_sz=5,
                 init_reg_param_log_space_lb=-2, init_reg_param_log_space_ub=1):
        self.th_id = th_id
        self.num_features = max_descent_steps
        self.num_cv_folds = num_cv_folds
        self.num_probations = num_probations
        self.max_probation_iters = max_probation_iters
        self.max_fmin_iters = max_fmin_iters
        self.ridge_function = ridge_f
        self.tolerance = tolerance
        self.max_succ_steps_no_improvement = max_succ_steps_no_improvement

        self.__default_seed_prime = 19

        self.__reg_params_sz = init_reg_param_log_space_sz
        self.__init_reg_params = np.logspace(init_reg_param_log_space_lb, init_reg_param_log_space_ub, init_reg_param_log_space_sz)

    def __set_rng_seed(self):
        np.random.seed(int(int(time.time()) + (os.getpid() + (self.th_id + 1) * self.__default_seed_prime) * self.__default_seed_prime))

    def __is_better_init_sol(self, current_obj, current_hparams, opt_obj, opt_hparams):
        obj_delta = (opt_obj - current_obj) / max(current_obj, opt_obj)
        if obj_delta > self.tolerance:
            return True
        obj_delta = abs(obj_delta)
        current_sol_norm = np.linalg.norm(self.ridge_function.spectrum(current_hparams))
        opt_sol_norm = np.linalg.norm(self.ridge_function.spectrum(opt_hparams))
        sol_norm_delta = (opt_sol_norm - current_sol_norm) / max(current_sol_norm, opt_sol_norm)
        if obj_delta < self.tolerance < sol_norm_delta:
            return True
        current_sol_reg_param = abs(self.ridge_function.regularization_param(current_hparams))
        opt_sol_reg_param = abs(self.ridge_function.regularization_param(opt_hparams))
        return obj_delta < self.tolerance and abs(sol_norm_delta) < self.tolerance and current_sol_reg_param > opt_sol_reg_param

    """
        Generate a sequence of ridge spectra by performing functional gradient descent through a set of ridge basis functions

        !!! This method is executed on a single core, but at the same time several instances of this method can run on different cores !!!

        Parameters:

            sm_X : multiprocessing RawArray
                   shared memory vector-buffer with concatenated training examples (instance by instance)

            sm_bias : multiprocessing RawArray
                      shared memory vector-buffer with current predictions at instances

            sm_y : multiprocessing RawArray
                   shared memory vector-buffer with target values for training examples

            split_args : numpy array
                         indices of selected data examples (the whole training set is not necessarily used)
                         shape=(s,), where s is the split size (defined by user)

            vl_mse : float
                     the current average mean squared error over validation samples in a k-fold cross-validation of ridge regression model

            dim : int
                  dimension of the problem

        Outputs:

            W : numpy array
                matrix with rows corresponding to ridge spectrum vectors
                shape=(t, d), where t is the number of constructed ridge spectra (t < @attributes: num_features) and d is the dimension of
                the problem
    """

    def generate(self, sm_X, sm_bias, sm_y, split_args, vl_mse, dim):
        self.__set_rng_seed()  # this is to avoid that different parallel processes generate identical random variables

        X, y = np.frombuffer(sm_X).reshape(-1, dim)[split_args, :], np.frombuffer(sm_y)[split_args]
        W, F = np.zeros((self.num_features, dim)), self.ridge_function.initialize_representation(X.shape[0], 1)
        F[:, 0] = np.copy(np.frombuffer(sm_bias)[split_args])
        active_feats, succ_steps_no_improvement = np.zeros(W.shape[0], dtype=bool), 0
        for i in range(self.num_features):
            if succ_steps_no_improvement == self.max_succ_steps_no_improvement:
                break

            cv_args, opt_val, opt_hparams = UnsupervisedKFold.kfolds(X.shape[0], self.num_cv_folds), 1e+10, None
            for j in range(self.num_probations):
                init_hparams = self.ridge_function.initialize_hparams(dim, self.__init_reg_params[j % self.__reg_params_sz])
                fmin_output = fmin_l_bfgs_b(lambda z: self.ridge_function.mse(z, X, y, cv_args, F[:, 0]), init_hparams,
                                            lambda z: self.ridge_function.mse_gradient(z, X, y, cv_args, F[:, 0]), disp=False,
                                            maxiter=self.max_probation_iters)
                if self.__is_better_init_sol(fmin_output[1], fmin_output[0], opt_val, opt_hparams):
                    opt_hparams, opt_val = fmin_output[0], fmin_output[1]

            fmin_output = fmin_l_bfgs_b(lambda z: self.ridge_function.mse(z, X, y, cv_args, F[:, 0]), opt_hparams,
                                        lambda z: self.ridge_function.mse_gradient(z, X, y, cv_args, F[:, 0]), disp=False,
                                        maxiter=self.max_fmin_iters)

            succ_diff = (fmin_output[1] - vl_mse) / max(vl_mse, fmin_output[1])
            if abs(succ_diff) < self.tolerance or succ_diff > 0:
                succ_steps_no_improvement += 1
                if succ_diff > self.tolerance:
                    continue  # the proposed ridge function points away from an optimum and it is, therefore, skipped
            else:
                succ_steps_no_improvement = 0

            W[i, :] = self.ridge_function.spectrum(fmin_output[0])
            F[:, 1:] = self.ridge_function.transform(X, W[i, :])[0]
            amplitudes = RidgeRegression.fit(F, y, self.ridge_function.regularization_param(fmin_output[0]))
            F[:, 0] = F.dot(amplitudes)
            vl_mse, active_feats[i] = fmin_output[1], True

        return W[np.argwhere(active_feats).reshape(-1), :]
