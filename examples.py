#!/usr/bin/python

############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import matplotlib.pyplot as pp
import logging

from sklearn import metrics

from ridge_function import (CosineRidge)
from gfc_actions import GFCRequest
from alc_actions import (AlaCarte, AlCRequest)
from feature_construction import (GreedyFeatureConstructor, AlaCarteFeatureConstructor)


logger_ids = ['RidgeRegression', 'ConfigureRequest', 'LoadDataAndConfigureRequest', 'GFCRequest', 'GFCConstructFeatureBlock',
              'GFCCommunicateFeatureBlock', 'GFCFitLinearModel', 'GFCFeatureSelection', 'GFCCommunicateGSGBias', 'GFCPrintStatistics',
              'GFCCheckConvergence', 'GFCMPICheckConvergence', 'GFCStoreModel', 'AlCRequest', 'AlCConstructFeatureBlock', 'AlCStoreModel',
              'GreedyFeatureConstructor', 'PickledDataGreedyFeatureConstructor', 'DistributedGreedyFeatureConstructor',
              'AlaCarteFeatureConstructor', 'PickledDataAlaCarteFeatureConstructor', 'examples']

example_logger = logging.getLogger('examples')


def configure_logger(logger_id, log_level=logging.DEBUG):
    logger = logging.getLogger(logger_id)
    logger.setLevel(log_level)
    logger.addHandler(logging.StreamHandler())


def start_loggers(log_level=logging.DEBUG):
    for logger_id in logger_ids:
        configure_logger(logger_id, log_level=log_level)


def sample_gp_f(gp_scale=2, num_samples=500, x_scale=5., noise_loc=0., noise_perc=0.05, scale_y=False, noisy_targets=True):
    x = np.sort(np.random.normal(scale=x_scale, size=num_samples)).reshape(-1, 1)

    pwd = metrics.pairwise.pairwise_distances(x)
    kmat = np.exp(-0.5 * (pwd ** 2) / (gp_scale ** 2))
    chol_dcmp = np.linalg.cholesky(kmat + 1e-10 * np.identity(num_samples))
    y = chol_dcmp.dot(np.random.normal(size=num_samples))

    if scale_y:
        y = (y - np.mean(y)) / (np.max(y) - np.min(y))

    if noisy_targets:
        noisy_y = y + np.random.normal(loc=noise_loc, scale=noise_perc * (np.max(y) - np.min(y)), size=num_samples)

    return x, y, noisy_y


def sample_mixture_gp_f(num_mixtures=5, num_mixture_cmps=10, x_scale=5., num_samples=500, noise_loc=0, noise_perc=0.02, scale_y=False,
                        noisy_targets=True, mixture_means_scale=2):
    x = np.sort(np.random.normal(scale=x_scale, size=num_samples)).reshape(-1, 1)

    mixture_means = mixture_means_scale * np.random.standard_normal(size=num_mixtures)
    mixture_scales = np.logspace(0, 1, 5)

    spectra = []
    for i in range(num_mixtures):
        spectra.append(np.random.normal(size=num_mixture_cmps, loc=mixture_means[i], scale=mixture_scales[i]))
    spectra = np.asarray(spectra).reshape(-1, 1)

    z = x.dot(spectra.T)
    u = np.zeros((x.shape[0], 2 * z.shape[1]))
    u[:, :z.shape[1]] = np.sin(z, u[:, :z.shape[1]])
    u[:, z.shape[1]:] = np.cos(z, u[:, z.shape[1]:])

    amp = np.random.standard_normal(size=u.shape[1])
    y = u.dot(amp)

    if scale_y:
        y = (y - np.mean(y)) / (np.max(y) - np.min(y))

    if noisy_targets:
        noisy_y = y + np.random.normal(loc=noise_loc, scale=noise_perc * (np.max(y) - np.min(y)), size=num_samples)

    return x, y, noisy_y, spectra


def outer_cv_splits(sz, perc_train=0.8):
    indices = np.arange(sz)
    np.random.shuffle(indices)
    split_marker = int(sz * perc_train)
    return indices[:split_marker], indices[split_marker:]


# !!! This is a multiprocessing call that runs min(2 * cpu_count, num_data_splits) parallel processes !!!
def gfc_fit_random_gp_function(rand_seed=0, gp_scale=2, noise_perc=0.05, plot_sz=1000, num_samples=500):
    np.random.seed(rand_seed)
    x, y, noisy_y = sample_gp_f(gp_scale=gp_scale, noise_perc=noise_perc, num_samples=num_samples)
    # x, y, noisy_y, rff_spectra = sample_mixture_gp_f(num_mixtures=5, num_samples=num_samples, noise_perc=noise_perc)
    train_args, test_args = outer_cv_splits(x.shape[0])

    ridge_f = CosineRidge()
    gfc_request = GFCRequest(max_lin_model_fits=1, gsg_ridge_f=ridge_f, gsg_max_descent_steps=25, split_sz=-1, num_data_splits=10,
                             feat_cut_off_fraction=1e-4, tolerance=1e-2, gsg_max_succ_steps_no_improvement=3)
    fc = GreedyFeatureConstructor(x, noisy_y, train_args, test_args)
    spectra, amplitudes = fc.construct(gfc_request)

    left_end, right_end = np.min(x), np.max(x)
    x_grid = np.linspace(left_end, right_end, plot_sz).reshape(-1, 1)
    mapped_x_grid = ridge_f.transform(x_grid, spectra.T, np.ones(plot_sz))[0]
    y_at_x_grid = mapped_x_grid.dot(amplitudes)
    pp.plot(x_grid.reshape(-1), y_at_x_grid.reshape(-1), color='b')
    pp.plot(x.reshape(-1), y, color='r')
    # pp.plot(x.reshape(-1), noisy_y, color='g')

    sorted_test_args = test_args[np.argsort(x[test_args, :].reshape(-1)).reshape(-1)]
    sorted_train_args = train_args[np.argsort(x[train_args, :].reshape(-1)).reshape(-1)]
    pp.scatter(x[sorted_train_args, :].reshape(-1), noisy_y[sorted_train_args], marker="s", s=4, color='y')
    pp.scatter(x[sorted_test_args, :].reshape(-1), noisy_y[sorted_test_args], marker="o", s=4, color='g')
    pp.show()


# !!! This is a multiprocessing call that runs min(cpu_count, num_inner_cv_folds) parallel processes !!!
def alc_fit_random_gp_function(rand_seed=0, gp_scale=2, noise_perc=0.05, plot_sz=1000, num_samples=500):
    np.random.seed(rand_seed)
    x, y, noisy_y = sample_gp_f(gp_scale=gp_scale, noise_perc=noise_perc, num_samples=num_samples)
    # x, y, noisy_y, rff_spectra = sample_mixture_gp_f(num_mixtures=5, num_samples=num_samples, noise_perc=noise_perc)
    train_args, test_args = outer_cv_splits(x.shape[0])

    alc_request = AlCRequest(num_components=5, num_feats_per_cmp=100, num_inner_cv_folds=5, num_probations=10, max_probation_iters=20,
                             max_fmin_iters=200)
    fc = AlaCarteFeatureConstructor(x, noisy_y, train_args, test_args)
    U, mu, sigma, nu, amplitudes = fc.construct(alc_request)

    left_end, right_end = np.min(x), np.max(x)
    x_grid = np.linspace(left_end, right_end, plot_sz).reshape(-1, 1)
    mapped_x_grid = AlaCarte.transform(x_grid, U, mu, sigma, nu)[0]
    y_at_x_grid = mapped_x_grid.dot(amplitudes)
    pp.plot(x_grid.reshape(-1), y_at_x_grid.reshape(-1), color='b')
    pp.plot(x.reshape(-1), y, color='r')
    # pp.plot(x.reshape(-1), noisy_y, color='g')

    sorted_test_args = test_args[np.argsort(x[test_args, :].reshape(-1)).reshape(-1)]
    sorted_train_args = train_args[np.argsort(x[train_args, :].reshape(-1)).reshape(-1)]
    pp.scatter(x[sorted_train_args, :].reshape(-1), noisy_y[sorted_train_args], marker="s", s=4, color='y')
    pp.scatter(x[sorted_test_args, :].reshape(-1), noisy_y[sorted_test_args], marker="o", s=4, color='g')
    pp.show()


if __name__ == '__main__':
    start_loggers()
    gfc_fit_random_gp_function(gp_scale=2, noise_perc=0.05, num_samples=500)
    alc_fit_random_gp_function(gp_scale=2, noise_perc=0.05, num_samples=500)
