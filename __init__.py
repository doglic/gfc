############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


from .linear_regression import (RidgeRegression, UnsupervisedKFold)

from .ridge_function import (RidgeFunction, CosineRidge, SpectrumRegularizedCosineRidge)

from .greedy_descent import GreedyDescent

from .fc_actions import (FCAction, FCRequest, ConfigureRequest, LoadDataAndConfigureRequest)

from .gfc_actions import (GFCRequest, GFCConstructFeatureBlock, GFCCommunicateFeatureBlock, GFCFitLinearModel, GFCFeatureSelection,
                          GFCCommunicateGSGBias, GFCPrintStatistics, GFCCheckConvergence, GFCMPICheckConvergence, GFCStoreModel)

from .alc_actions import (AlaCarte, AlCRequest, AlCConstructFeatureBlock, AlCStoreModel)

from .feature_construction import (AbstractFC, GreedyFeatureConstructor, PickledDataGreedyFeatureConstructor,
                                   DistributedGreedyFeatureConstructor, AlaCarteFeatureConstructor, PickledDataAlaCarteFeatureConstructor)

from .tests import GFCUnitTests

__all__ = ['RidgeRegression',
           'UnsupervisedKFold',
           'RidgeFunction',
           'SpectrumRegularizedCosineRidge',
           'CosineRidge',
           'GreedyDescent',
           'FCAction',
           'FCRequest',
           'ConfigureRequest',
           'LoadDataAndConfigureRequest',
           'GFCRequest',
           'GFCConstructFeatureBlock',
           'GFCCommunicateFeatureBlock',
           'GFCFitLinearModel',
           'GFCFeatureSelection',
           'GFCCommunicateGSGBias',
           'GFCPrintStatistics',
           'GFCCheckConvergence',
           'GFCMPICheckConvergence',
           'GFCStoreModel',
           'AlaCarte',
           'AlCRequest',
           'AlCConstructFeatureBlock',
           'AlCStoreModel',
           'AbstractFC',
           'GreedyFeatureConstructor',
           'PickledDataGreedyFeatureConstructor',
           'DistributedGreedyFeatureConstructor',
           'AlaCarteFeatureConstructor',
           'PickledDataAlaCarteFeatureConstructor',
           'GFCUnitTests']
