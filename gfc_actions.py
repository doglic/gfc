############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import multiprocessing as mp
import ctypes as ct
import pickle
import logging

from mpi4py.MPI import (ANY_SOURCE, ANY_TAG)

from fc_actions import (FCRequest, FCAction)
from ridge_function import CosineRidge
from greedy_descent import GreedyDescent
from linear_regression import RidgeRegression


class GFCRequest(FCRequest):

    def __init__(self, max_lin_model_fits=1, num_data_splits=10, split_sz=None, tolerance=1e-2, feat_cut_off_fraction=1e-4,
                 lm_init_reg_param=1e-2, num_inner_cv_folds=5, gsg_max_descent_steps=20, gsg_num_probations=10,
                 gsg_max_probation_iters=20, gsg_max_fmin_iters=200, gsg_ridge_f=CosineRidge(), gsg_max_succ_steps_no_improvement=3):

        self.logger = logging.getLogger('GFCRequest')

        self.max_lin_model_fits = max_lin_model_fits
        self.num_data_splits = num_data_splits
        self.max_mp_pool_threads = min(num_data_splits, 2 * mp.cpu_count())
        self.split_sz = split_sz
        self.num_inner_cv_folds = num_inner_cv_folds

        self.feat_cut_off_fraction = feat_cut_off_fraction
        self.feat_cut_off = None
        self.tolerance = tolerance

        self.lm_init_reg_param = np.array([lm_init_reg_param])
        self.lm_vl_mse = 1e+10
        self.lm_vl_mse_succ_diff = None

        self.gsg_max_descent_steps = gsg_max_descent_steps
        self.gsg_num_cv_folds = num_inner_cv_folds
        self.gsg_num_probations = gsg_num_probations
        self.gsg_max_probation_iters = gsg_max_probation_iters
        self.gsg_max_fmin_iters = gsg_max_fmin_iters
        self.gsg_ridge_f = gsg_ridge_f
        self.gsg_tolerance = tolerance
        self.gsg_max_succ_steps_no_improvement = gsg_max_succ_steps_no_improvement
        self.gsg_bias_buffer = None

        self.sm_W = None
        self.sm_gsg_bias = None

        self.active_spectra_indices = None
        self.active_amp_indices = None
        self.constructed_spectrum_block_sz = 0

        self.has_converged = False

        self.logger.debug('max number of linear model fits: ' + str(self.max_lin_model_fits))
        self.logger.debug('number of data splits (per machine): ' + str(self.num_data_splits))
        self.logger.debug('max number of multiprocessing threads (used cores per machine): ' + str(self.max_mp_pool_threads))
        self.logger.debug('size of a data split: ' + str(self.split_sz))
        self.logger.debug('tolerance (x100 = improvement in percentages): ' + str(self.tolerance))
        self.logger.debug('initial value of the regularization parameter (linear ridge regression): ' + str(self.lm_init_reg_param))
        self.logger.debug('number of folds in the inner cross-validation (used with linear ridge regression): ' +
                          str(self.num_inner_cv_folds))

        self.logger.debug('[greedy sequence generator] max descent steps: ' + str(self.gsg_max_descent_steps))
        self.logger.debug('[greedy sequence generator] number of folds in the inner cross-validation: ' + str(self.gsg_num_cv_folds))
        self.logger.debug('[greedy sequence generator] number of init-solution probations: ' + str(self.gsg_num_probations))
        self.logger.debug('[greedy sequence generator] max iterations per probation: ' + str(self.gsg_max_probation_iters))
        self.logger.debug('[greedy sequence generator] max fmin iterations (after best probation): ' + str(self.gsg_max_fmin_iters))
        self.logger.debug('[greedy sequence generator] tolerance (x100 = improvement in percentages): ' + str(self.gsg_tolerance))
        self.logger.debug('[greedy sequence generator] max successive steps without improvement: ' +
                          str(self.gsg_max_succ_steps_no_improvement))

    def reset(self):
        super(GFCRequest, self).reset()

        self.lm_vl_mse = 1e+10
        self.lm_vl_mse_succ_diff = None

        self.sm_W = None
        self.sm_gsg_bias = None
        self.gsg_bias_buffer = None

        self.feat_cut_off = None
        self.active_spectra_indices = None
        self.active_amp_indices = None
        self.constructed_spectrum_block_sz = 0
        self.has_converged = False

    def configure(self, X, y, train_args, test_args):
        self.sm_train_X = FCRequest.to_shared_memory(X[train_args, :])
        self.sm_train_y = FCRequest.to_shared_memory(y[train_args])
        self.sm_test_X = FCRequest.to_shared_memory(X[test_args, :])
        self.sm_test_y = FCRequest.to_shared_memory(y[test_args])

        self.train_sz = train_args.shape[0]
        self.test_sz = test_args.shape[0]
        self.dim = X.shape[1]

        self.sm_W = mp.RawArray(ct.c_double, 0)
        self.sm_train_F = FCRequest.to_shared_memory(np.ones(self.train_sz))
        self.sm_test_F = FCRequest.to_shared_memory(np.ones(self.test_sz))

        self.active_spectra_indices = np.array([], dtype=np.int)
        self.active_amp_indices = np.array([0], dtype=np.int)
        self.amplitudes = np.array([np.mean(y[train_args])])
        self.sm_gsg_bias = FCRequest.to_shared_memory(np.ones(self.train_sz) * self.amplitudes[0])
        self.lm_vl_mse = np.mean((y[train_args] - self.amplitudes[0]) ** 2)

        self.test_y_variance = np.mean((y[test_args] - np.mean(y[test_args])) ** 2)
        self.feat_cut_off = self.feat_cut_off_fraction * (np.max(y[train_args]) - np.min(y[train_args]))

        self.set_inner_cv_folds()

        self.logger.debug('feature cut-off: ' + str(self.feat_cut_off))

    '''
        The method appends spectrum vectors constructed in an instance of greedy descent to the existing set of (non-redundant) spectra
        (shared memory block is updated as a result of this call)

        Parameters:

            constructed_spectra : numpy array
                                  shape=(@parameter: constructed_spectra_sz, d), where d is the dimension of the problem

            constructed_spectra_sz : int
                                     number of ridge spectra constructed in an instance of greedy descent
    '''
    def update_sm_spectra(self, constructed_spectra, constructed_spectra_sz):
        W = np.frombuffer(self.sm_W).reshape(-1, self.dim)[self.active_spectra_indices, :]
        updated_sm_W = mp.RawArray(ct.c_double, (W.shape[0] + constructed_spectra_sz) * self.dim)
        updated_W = np.frombuffer(updated_sm_W).reshape(-1, self.dim)
        updated_W[:W.shape[0], :] = W

        row_cursor = W.shape[0]
        for fblock in constructed_spectra:
            row_start, row_end = row_cursor, row_cursor + fblock.shape[0]
            updated_W[row_start:row_end, :] = fblock
            row_cursor = row_end

        del W, self.sm_W
        self.sm_W = updated_sm_W
        self.active_spectra_indices = np.arange(updated_W.shape[0])

    def __update_sm_feature_space(self, sm_F, sm_X):
        X = np.frombuffer(sm_X).reshape(-1, self.dim)
        F = np.frombuffer(sm_F).reshape(X.shape[0], -1)[:, self.active_amp_indices]
        W = np.frombuffer(self.sm_W).reshape(-1, self.dim)
        prev_spectra_sz = self.gsg_ridge_f.spectra_dim(F.shape[1], True)
        updated_sm_F = mp.RawArray(ct.c_double, X.shape[0] * self.gsg_ridge_f.representation_dim(W.shape[0], True))
        updated_F = np.frombuffer(updated_sm_F).reshape(X.shape[0], -1)
        updated_F[:, :F.shape[1]] = F
        updated_F[:, F.shape[1]:] = self.gsg_ridge_f.transform(X, W[prev_spectra_sz:, :].T)[0]
        return updated_sm_F

    '''
        The method updates the shared memory block with the feature representation defined by the constructed ridge spectrum vectors
    '''
    def update_sm_F(self):
        self.sm_train_F = self.__update_sm_feature_space(self.sm_train_F, self.sm_train_X)
        self.sm_test_F = self.__update_sm_feature_space(self.sm_test_F, self.sm_test_X)

    '''
        The method marks active spectrum vectors and corresponding ridge features by comparing the values of the amplitudes to the cut-off
    '''
    def update_active_features(self):
        self.active_spectra_indices = self.gsg_ridge_f.active_spectra_indices(self.amplitudes[1:], self.feat_cut_off)
        self.active_amp_indices = np.append(0, 1 + self.gsg_ridge_f.amplitude_indices(self.active_spectra_indices))


def mp_pool_init(sm_train_X, sm_gsg_bias, sm_train_y):
    global mp_X, mp_bias, mp_y
    mp_X, mp_bias, mp_y = sm_train_X, sm_gsg_bias, sm_train_y


def generate_greedy_spectra_sequence(th_id, split_args, dim, ridge_f, max_descent_steps, num_cv_folds, num_probations, max_probation_iters,
                                     max_fmin_iters, lm_vl_mse, tolerance, max_steps_with_no_improvement):
    gsg = GreedyDescent(th_id=th_id, max_descent_steps=max_descent_steps, num_cv_folds=num_cv_folds, num_probations=num_probations,
                        max_probation_iters=max_probation_iters, max_fmin_iters=max_fmin_iters, ridge_f=ridge_f, tolerance=tolerance,
                        max_succ_steps_no_improvement=max_steps_with_no_improvement)
    return gsg.generate(mp_X, mp_bias, mp_y, split_args, lm_vl_mse, dim)


class GFCConstructFeatureBlock(FCAction):

    def __init__(self, next_action=None):
        self.next_action = next_action
        self.logger = logging.getLogger('GFCConstructFeatureBlock')

    @staticmethod
    def _split_args(sz, num_splits, split_sz):
        indices, split_args = np.arange(sz), []
        np.random.shuffle(indices)
        if split_sz is None:
            split_blocks = (sz / num_splits) * np.ones(num_splits, dtype=np.int)
            split_blocks[:sz % num_splits] += 1
            split_cursor = 0
            for block_sz in split_blocks:
                start_index, end_index = split_cursor, split_cursor + block_sz
                split_args.append(indices[start_index:end_index])
                split_cursor = end_index
        elif num_splits == 1:
            split_args.append(indices)
        else:
            for i in range(num_splits):
                np.random.shuffle(indices)
                if split_sz == -1:
                    split_args.append(indices)
                else:
                    split_args.append(indices[:split_sz])
        return split_args

    def _process_request(self, gfc_request):
        self.logger.info('####################################################################################################')
        split_args = GFCConstructFeatureBlock._split_args(gfc_request.train_sz, gfc_request.num_data_splits, gfc_request.split_sz)
        processor_pool = mp.Pool(gfc_request.max_mp_pool_threads, initializer=mp_pool_init,
                                 initargs=(gfc_request.sm_train_X, gfc_request.sm_gsg_bias, gfc_request.sm_train_y))
        async_results = []
        for th_id, th_args in enumerate(split_args):
            async_results.append(processor_pool.apply_async(generate_greedy_spectra_sequence,
                                                            (th_id, th_args, gfc_request.dim, gfc_request.gsg_ridge_f,
                                                             gfc_request.gsg_max_descent_steps, gfc_request.gsg_num_cv_folds,
                                                             gfc_request.gsg_num_probations, gfc_request.gsg_max_probation_iters,
                                                             gfc_request.gsg_max_fmin_iters, gfc_request.lm_vl_mse, gfc_request.tolerance,
                                                             gfc_request.gsg_max_succ_steps_no_improvement)))
        processor_pool.close()
        processor_pool.join()

        constructed_spectra, constructed_spectra_sz = [], 0
        for j in range(gfc_request.num_data_splits):
            feat_block = async_results[j].get()
            constructed_spectra.append(feat_block)
            constructed_spectra_sz += feat_block.shape[0]
        del async_results, split_args
        self.logger.info('[multiprocessing pool] size of the constructed spectrum block: ' + str(constructed_spectra_sz))

        gfc_request.constructed_spectrum_block_sz = constructed_spectra_sz
        if gfc_request.constructed_spectrum_block_sz > 0:
            gfc_request.update_sm_spectra(constructed_spectra, constructed_spectra_sz)
        del constructed_spectra


class GFCCommunicateFeatureBlock(FCAction):

    def __init__(self, communicator, mpi_root_id=0, next_action=None):
        self.next_action = next_action
        self.logger = logging.getLogger('GFCCommunicateFeatureBlock')

        self.communicator = communicator
        self.mpi_machine_id = communicator.Get_rank()
        self.mpi_world_sz = communicator.Get_size()
        self.mpi_root_id = mpi_root_id

    def _is_root(self):
        return self.mpi_machine_id == self.mpi_root_id

    @staticmethod
    def _remove_reduntant_buffer_entries(recv_buffer, dim):
        recv_buffer = recv_buffer.reshape(-1, dim)
        abs_row_sums = np.sum(np.abs(recv_buffer), axis=1).reshape(-1)
        active_rows = np.argwhere(abs_row_sums != 0).reshape(-1)
        return recv_buffer[active_rows, :]

    def _process_request(self, gfc_request):
        if self._is_root():
            constructed_spectra, constructed_spectra_sz = [], 0
            for i in range(1, self.mpi_world_sz):
                recv_buffer = np.zeros(gfc_request.dim * gfc_request.num_data_splits * gfc_request.gsg_max_descent_steps, dtype=np.float64)
                self.communicator.Recv(recv_buffer, source=ANY_SOURCE, tag=ANY_TAG)
                recv_buffer = GFCCommunicateFeatureBlock._remove_reduntant_buffer_entries(recv_buffer, gfc_request.dim)
                constructed_spectra.append(recv_buffer)
                constructed_spectra_sz += recv_buffer.shape[0]
            gfc_request.constructed_spectrum_block_sz += constructed_spectra_sz

            if constructed_spectra_sz > 0:
                gfc_request.update_sm_spectra(constructed_spectra, constructed_spectra_sz)
            del constructed_spectra
        else:
            W = np.frombuffer(gfc_request.sm_W).reshape(-1)
            self.communicator.Send(W, dest=self.mpi_root_id)

            del W, gfc_request.sm_W
            gfc_request.active_spectra_indices = np.array([], dtype=np.int)
            gfc_request.sm_W = mp.RawArray(ct.c_double, 0)
            gfc_request.constructed_spectrum_block_sz = 0


class GFCFitLinearModel(FCAction):

    def __init__(self, next_action=None):
        self.next_action = next_action
        self.logger = logging.getLogger('GFCFitLinearModel')

    def _process_request(self, gfc_request):
        self.logger.info('size of the constructed spectrum block: ' + str(gfc_request.constructed_spectrum_block_sz))
        if gfc_request.constructed_spectrum_block_sz == 0:
            self.logger.info('no progress achieved with additional greedy descent steps! exiting the feature construction...')
            return

        gfc_request.update_sm_F()
        gfc_request.lm_opt_reg_param, vl_mse = RidgeRegression.kfold_fit(gfc_request.sm_train_F, gfc_request.sm_train_y,
                                                                         gfc_request.inner_cv_args, gfc_request.lm_init_reg_param)
        gfc_request.lm_vl_mse_succ_diff = (vl_mse - gfc_request.lm_vl_mse) / max(vl_mse, gfc_request.lm_vl_mse)
        gfc_request.lm_vl_mse = vl_mse

        train_F = np.frombuffer(gfc_request.sm_train_F).reshape(gfc_request.train_sz, -1)
        train_y = np.frombuffer(gfc_request.sm_train_y).reshape(-1)
        gfc_request.amplitudes = RidgeRegression.fit(train_F, train_y, gfc_request.lm_opt_reg_param)
        gfc_request.active_amp_indices = np.arange(gfc_request.amplitudes.shape[0])

        bias = np.frombuffer(gfc_request.sm_gsg_bias)
        bias[:] = train_F.dot(gfc_request.amplitudes)

    def _forward_request(self, gfc_request):
        if gfc_request.constructed_spectrum_block_sz == 0:
            return
        super(GFCFitLinearModel, self)._forward_request(gfc_request)


class GFCFeatureSelection(FCAction):

    def __init__(self, next_action=None):
        self.next_action = next_action
        self.logger = logging.getLogger('GFCFeatureSelection')

    def _process_request(self, gfc_request):
        train_F = np.frombuffer(gfc_request.sm_train_F).reshape(gfc_request.train_sz, -1)
        train_y = np.frombuffer(gfc_request.sm_train_y).reshape(-1)
        test_F = np.frombuffer(gfc_request.sm_test_F).reshape(gfc_request.test_sz, -1)
        test_y = np.frombuffer(gfc_request.sm_test_y).reshape(-1)
        test_residue = test_F.dot(gfc_request.amplitudes) - test_y
        spectra_sz = gfc_request.active_spectra_indices.shape[0]
        self.logger.info('[before feature selection] active (non-redundant) spectrum vectors: ' + str(spectra_sz))
        self.logger.info('[before feature selection] test RMSE: ' + str(np.sqrt(np.mean(test_residue ** 2))))

        gfc_request.update_active_features()
        if gfc_request.active_spectra_indices.shape[0] != spectra_sz:
            self.logger.info('--> removed ' + str(spectra_sz - gfc_request.active_spectra_indices.shape[0]) + ' spectrum vectors')
            gfc_request.amplitudes[gfc_request.active_amp_indices] = RidgeRegression.fit(train_F[:, gfc_request.active_amp_indices],
                                                                                         train_y, gfc_request.lm_opt_reg_param)
            self.logger.info('[after feature selection] active spectrum vectors: ' + str(gfc_request.active_spectra_indices.shape[0]))

        bias = np.frombuffer(gfc_request.sm_gsg_bias)
        bias[:] = train_F[:, gfc_request.active_amp_indices].dot(gfc_request.amplitudes[gfc_request.active_amp_indices])


class GFCCommunicateGSGBias(FCAction):

    def __init__(self, communicator, mpi_root_id=0, next_action=None):
        self.next_action = next_action
        self.logger = logging.getLogger('GFCCommunicateGSGBias')

        self.communicator = communicator
        self.mpi_machine_id = communicator.Get_rank()
        self.mpi_world_sz = communicator.Get_size()
        self.mpi_root_id = mpi_root_id

    def _is_not_root(self):
        return self.mpi_machine_id != self.mpi_root_id

    def _process_request(self, gfc_request):
        gfc_request.gsg_bias_buffer = np.copy(np.frombuffer(gfc_request.sm_gsg_bias))
        self.communicator.Bcast(gfc_request.gsg_bias_buffer, root=self.mpi_root_id)

        if self._is_not_root():
            del gfc_request.sm_gsg_bias
            gfc_request.sm_gsg_bias = mp.RawArray(ct.c_double, gfc_request.gsg_bias_buffer.shape[0])
            bias = np.frombuffer(gfc_request.sm_gsg_bias)
            bias[:] = gfc_request.gsg_bias_buffer
            gfc_request.gsg_bias_buffer = None


class GFCPrintStatistics(FCAction):

    def __init__(self, next_action=None):
        self.next_action = next_action
        self.logger = logging.getLogger('GFCPrintStatistics')

    def _process_request(self, gfc_request):
        train_y = np.frombuffer(gfc_request.sm_train_y).reshape(-1)
        bias = np.frombuffer(gfc_request.sm_gsg_bias).reshape(-1)
        train_residue = train_y - bias
        gfc_request.log_residue_stats(train_residue)

        test_F = np.frombuffer(gfc_request.sm_test_F).reshape(gfc_request.test_sz, -1)
        test_y = np.frombuffer(gfc_request.sm_test_y)
        test_residue = test_y - test_F[:, gfc_request.active_amp_indices].dot(gfc_request.amplitudes[gfc_request.active_amp_indices])
        gfc_request.log_residue_stats(test_residue, 'test')
        gfc_request.log_r2(test_residue)


class GFCCheckConvergence(FCAction):

    def __init__(self, next_action=None):
        self.next_action = next_action
        self.logger = logging.getLogger('GFCCheckConvergence')
        self.check_counter = 0

    def _process_request(self, gfc_request):
        gfc_request.has_converged = self._has_converged(gfc_request)
        self.check_counter += 1

    def _has_converged(self, gfc_request):
        total_num_threads = gfc_request.num_data_splits  # executed on a single machine
        return abs(gfc_request.lm_vl_mse_succ_diff) < gfc_request.tolerance or gfc_request.constructed_spectrum_block_sz < total_num_threads

    def _forward_request(self, gfc_request):
        if gfc_request.lm_vl_mse_succ_diff is not None:  # in the MPI implementation of the check this information is not communicated
            self.logger.info('the successive difference in the validation error is equal to ' +
                             str(100 * gfc_request.lm_vl_mse_succ_diff) + '%')
        if gfc_request.has_converged:
            self.logger.info('the improvement is less than ' + str(100 * gfc_request.tolerance) +
                             '% or the number of constructed spectrum vectors is less than the number of \nused threads! exiting the ' +
                             'feature construction...')
            return
        elif self.check_counter == gfc_request.max_lin_model_fits:
            self.logger.info('max number of linear models fitted! exiting the feature construction...')
            return
        super(GFCCheckConvergence, self)._forward_request(gfc_request)


class GFCMPICheckConvergence(GFCCheckConvergence):

    def __init__(self, communicator, mpi_root_id=0, next_action=None):
        super(GFCMPICheckConvergence, self).__init__(next_action)
        self.logger = logging.getLogger('GFCMPICheckConvergence')

        self.communicator = communicator
        self.mpi_machine_id = communicator.Get_rank()
        self.mpi_world_sz = communicator.Get_size()
        self.mpi_root_id = mpi_root_id

    def _is_root(self):
        return self.mpi_machine_id == self.mpi_root_id

    def _has_converged(self, gfc_request):
        total_num_threads = gfc_request.num_data_splits * self.mpi_world_sz
        return abs(gfc_request.lm_vl_mse_succ_diff) < gfc_request.tolerance or gfc_request.constructed_spectrum_block_sz < total_num_threads

    def _process_request(self, gfc_request):
        if self._is_root():
            gfc_request.has_converged = self._has_converged(gfc_request)
        self.communicator.bcast(gfc_request.has_converged, root=self.mpi_root_id)
        self.check_counter += 1


class GFCStoreModel(FCAction):

    def __init__(self, model_path, train_args, test_args):
        self.next_action = None
        self.logger = logging.getLogger('GFCStoreModel')

        self.model_path = model_path
        self.train_args = train_args
        self.test_args = test_args

    def _process_request(self, gfc_request):
        f = open(self.model_path, 'wb')
        W = np.frombuffer(gfc_request.sm_W).reshape(-1, gfc_request.dim)[gfc_request.active_spectra_indices, :]
        amplitudes = gfc_request.amplitudes[gfc_request.active_amp_indices]
        pickle.dump((W, amplitudes, self.train_args, self.test_args), f)
        f.close()
