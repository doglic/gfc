############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import multiprocessing as mp
import ctypes as ct
import pickle
import time
import logging

from abc import (ABCMeta, abstractmethod)

from linear_regression import UnsupervisedKFold


class FCRequest(object):

    __metaclass__ = ABCMeta

    logger = logging.getLogger('FCRequest')

    train_sz = None
    test_sz = None
    dim = None

    sm_train_X = None
    sm_train_y = None
    sm_train_F = None

    sm_test_X = None
    sm_test_y = None
    sm_test_F = None
    test_y_variance = None

    amplitudes = None
    lm_init_reg_param = None
    lm_opt_reg_param = None

    num_inner_cv_folds = None
    inner_cv_args = None

    '''
        The method resets shared memory blocks where data dependent variables are stored
    '''
    @abstractmethod
    def reset(self):
        self.train_sz = None
        self.test_sz = None
        self.dim = None

        self.sm_train_X = None
        self.sm_train_y = None
        self.sm_train_F = None

        self.sm_test_X = None
        self.sm_test_y = None
        self.sm_test_F = None
        self.test_y_variance = None

        self.amplitudes = None
        self.lm_opt_reg_param = None
        self.inner_cv_args = None

    '''
        The method configures request by loading data into the shared memory blocks
    '''
    @abstractmethod
    def configure(self, X, y, train_args, test_args):
        pass

    '''
        The method prints the number of instances, dimension of the problem, and range of the output variable
    '''
    def print_data_stats(self, X, y):
        self.logger.debug('X shape: ' + str(X.shape))
        self.logger.debug('y shape: ' + str(y.shape))
        self.logger.debug('y range: ' + str(np.max(y) - np.min(y)))

    '''
        The method converts a numpy array to a multiprocessing RawArray (memory block that can be shared between processes)

        Parameters:

            source_arr : numpy array
                         shape=(p, q)

        Outputs:

            mp_array : multiprocessing RawArray
                       size=pq
    '''
    @staticmethod
    def to_shared_memory(source_arr):
        source_arr = source_arr.reshape(-1)
        mp_array = mp.RawArray(ct.c_double, source_arr.shape[0])
        np_array = np.frombuffer(mp_array)
        np_array[:] = source_arr
        return mp_array

    '''
        The method pickles data from a binary file

        Parameters:

            data_path : string
                        path to a binary file that contains the data as a tuple (X, y), where X is a two and y a one dimensional numpy array

            scale_y : bool
                      indicator specifying whether the outputs should be centered and scaled so that their range is equal to one

        Outputs:

            X : numpy array
                matrix with data instances as rows
                shape=(m, d), where m is the number of instances and d is the dimension of the problem

            y : numpy array
                vector with target outputs
                shape=(m,), where m is the number of data instances
    '''
    @staticmethod
    def load_data(data_path, scale_y=True):
        f = open(data_path, 'rb')
        X, y = pickle.load(f)
        f.close()

        if scale_y:
            y = (y - np.mean(y)) / (np.max(y) - np.min(y))

        return X, y

    '''
        The method pickles data from a binary file and configures the data dependent variables of the request

        Parameters:

            data_path : string
                        path to a binary file that contains the data as a tuple (X, y), where X is a two and y a one dimensional numpy array

            train_args : numpy array
                         indices of instances to be used as training examples
                         shape=(s,)

            test_args : numpy array
                        indices of instances to be used as test examples
                        shape=(t,), where s + t = m (the total number of data instances)

            scale_y : bool
                      indicator specifying whether the outputs should be centered and scaled so that their range is equal to one
    '''
    def load_data_and_configure(self, data_path, train_args, test_args, scale_y=True):
        X, y = FCRequest.load_data(data_path, scale_y)
        self.print_data_stats(X, y)
        self.configure(X, y, train_args, test_args)

    '''
        The method generates inner cross-validation folds to be used in k-fold hyperparameter fitting
    '''
    def set_inner_cv_folds(self):
        self.inner_cv_args = UnsupervisedKFold.kfolds(self.train_sz, self.num_inner_cv_folds)

    '''
        The method computes the statistical measure of quality of fit -- cross-validated R^2 value
        (the statistics is computed only over test samples)

        Parameters:

            test_residuals : numpy array
                             shape=(m,), where m is the number of instances

        Outputs:

            r2 : float
    '''
    def r2(self, test_residuals):
        return 1 - np.mean(test_residuals ** 2) / self.test_y_variance

    def log_residue_stats(self, residue, prefix='train'):
        abs_residue = np.abs(residue)
        self.logger.info('--> ' + prefix + ' RMSE: ' + str(np.sqrt(np.mean(residue ** 2))))
        self.logger.info('--> ' + prefix + ' residue interval: [' + str(np.min(abs_residue)) + ', ' + str(np.max(abs_residue)) + ']')

    def log_r2(self, test_residue):
        self.logger.info('--> test R2: ' + str(self.r2(test_residue)))


class FCAction(object):

    __metaclass__ = ABCMeta

    logger = logging.getLogger('FCAction')

    next_action = None

    @abstractmethod
    def _process_request(self, fc_request):
        pass

    def _forward_request(self, fc_request):
        if self.next_action is not None:
            self.next_action.execute(fc_request)

    def execute(self, fc_request):
        action_start_t = time.time()
        self._process_request(fc_request)
        self.logger.info('TIME: ' + str(time.time() - action_start_t))

        self._forward_request(fc_request)

    def set_next_action(self, next_act):
        self.next_action = next_act


class ConfigureRequest(FCAction):

    def __init__(self, X, y, train_args, test_args, next_action=None):
        self.next_action = next_action
        self.logger = logging.getLogger('ConfigureRequest')

        self.X = X
        self.y = y
        self.train_args = train_args
        self.test_args = test_args

    def _process_request(self, fc_request):
        fc_request.reset()
        fc_request.configure(self.X, self.y, self.train_args, self.test_args)


class LoadDataAndConfigureRequest(FCAction):

    def __init__(self, data_path, train_args, test_args, scale_y=True, next_action=None):
        self.next_action = next_action
        self.logger = logging.getLogger('LoadDataAndConfigureRequest')

        self.data_path = data_path
        self.train_args = train_args
        self.test_args = test_args
        self.scale_y = scale_y

    def _process_request(self, fc_request):
        fc_request.reset()
        fc_request.load_data_and_configure(self.data_path, self.train_args, self.test_args, self.scale_y)
