############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import logging

from abc import (ABCMeta, abstractmethod)
from mpi4py import MPI

from fc_actions import (FCAction, LoadDataAndConfigureRequest, ConfigureRequest)
from alc_actions import (AlCConstructFeatureBlock, AlCStoreModel)
from gfc_actions import (GFCConstructFeatureBlock, GFCFitLinearModel, GFCStoreModel, GFCFeatureSelection, GFCPrintStatistics,
                         GFCCommunicateGSGBias, GFCCommunicateFeatureBlock, GFCCheckConvergence, GFCMPICheckConvergence)


class AbstractFC(FCAction):

    __metaclass__ = ABCMeta

    action = None

    def _process_request(self, fc_request):
        self.action.execute(fc_request)
        self.logger.info('####################################################################################################')

    @abstractmethod
    def _configure_action_chain(self):
        pass

    @abstractmethod
    def construct(self, fc_request):
        self.execute(fc_request)


class GreedyFeatureConstructor(AbstractFC):

    def __init__(self, X, y, train_args, test_args, model_path=None):
        self.logger = logging.getLogger('GreedyFeatureConstructor')
        self._configure_next_action(model_path, train_args, test_args)
        self.action = ConfigureRequest(X, y, train_args, test_args)
        self._configure_action_chain()

    def _configure_action_chain(self):
        feature_block_action = GFCConstructFeatureBlock()
        lm_fit_action = GFCFitLinearModel()
        feature_selection_action = GFCFeatureSelection()
        print_stats_action = GFCPrintStatistics()
        check_convergence_action = GFCCheckConvergence()

        check_convergence_action.set_next_action(feature_block_action)
        print_stats_action.set_next_action(check_convergence_action)
        feature_selection_action.set_next_action(print_stats_action)
        lm_fit_action.set_next_action(feature_selection_action)
        feature_block_action.set_next_action(lm_fit_action)
        self.action.set_next_action(feature_block_action)

    def _configure_next_action(self, model_path, train_args, test_args):
        if model_path is not None:
            self.next_action = GFCStoreModel(model_path, train_args, test_args)
        else:
            self.next_action = None

    def construct(self, gfc_request):
        super(GreedyFeatureConstructor, self).construct(gfc_request)
        W = np.frombuffer(gfc_request.sm_W).reshape(-1, gfc_request.dim)[gfc_request.active_spectra_indices, :]
        amplitudes = gfc_request.amplitudes[gfc_request.active_amp_indices]
        return W, amplitudes


class PickledDataGreedyFeatureConstructor(GreedyFeatureConstructor):

    def __init__(self, data_path, train_args, test_args, scale_y=True, model_path=None):
        self.logger = logging.getLogger('PickledDataGreedyFeatureConstructor')
        self._configure_next_action(model_path, train_args, test_args)

        self.action = LoadDataAndConfigureRequest(data_path, train_args, test_args, scale_y)
        self._configure_action_chain()


class DistributedGreedyFeatureConstructor(PickledDataGreedyFeatureConstructor):

    def __init__(self, data_path, train_args, test_args, scale_y=True, model_path=None, mpi_root_id=0):
        self.logger = logging.getLogger('DistributedGreedyFeatureConstructor')

        self.communicator = MPI.COMM_WORLD
        self.mpi_machine_id = self.communicator.Get_rank()
        self.mpi_root_id = mpi_root_id

        super(DistributedGreedyFeatureConstructor, self).__init__(data_path, train_args, test_args, scale_y, model_path)
        self.logger.info('MPI world size: ' + str(self.communicator.Get_size()))

    def _is_root(self):
        return self.mpi_root_id == self.mpi_machine_id

    def _configure_next_action(self, model_path, train_args, test_args):
        if self._is_root():
            super(DistributedGreedyFeatureConstructor, self)._configure_next_action(model_path, train_args, test_args)

    def _configure_action_chain(self):
        feature_block_action = GFCConstructFeatureBlock()
        communicate_feat_block_action = GFCCommunicateFeatureBlock(self.communicator)
        lm_fit_action = GFCFitLinearModel()
        feature_selection_action = GFCFeatureSelection()
        communicate_gsg_bias_action = GFCCommunicateGSGBias(self.communicator)
        print_stats_action = GFCPrintStatistics()
        check_convergence_action = GFCMPICheckConvergence(self.communicator)

        check_convergence_action.set_next_action(feature_block_action)
        if self._is_root():
            print_stats_action.set_next_action(check_convergence_action)
            communicate_gsg_bias_action.set_next_action(print_stats_action)
            feature_selection_action.set_next_action(communicate_gsg_bias_action)
            lm_fit_action.set_next_action(feature_selection_action)
            communicate_feat_block_action.set_next_action(lm_fit_action)
        else:
            communicate_gsg_bias_action.set_next_action(check_convergence_action)
            communicate_feat_block_action.set_next_action(communicate_gsg_bias_action)
        feature_block_action.set_next_action(communicate_feat_block_action)
        self.action.set_next_action(feature_block_action)


class AlaCarteFeatureConstructor(AbstractFC):

    def __init__(self, X, y, train_args, test_args, model_path=None):
        self.logger = logging.getLogger('AlaCarteFeatureConstructor')
        self._configure_next_action(model_path, train_args, test_args)
        self.action = ConfigureRequest(X, y, train_args, test_args)
        self._configure_action_chain()

    def _configure_next_action(self, model_path, train_args, test_args):
        if model_path is not None:
            self.next_action = AlCStoreModel(model_path, train_args, test_args)
        else:
            self.next_action = None

    def construct(self, alc_request):
        super(AlaCarteFeatureConstructor, self).execute(alc_request)
        U = np.frombuffer(alc_request.sm_U).reshape(-1, alc_request.dim)
        return U, alc_request.opt_mu, alc_request.opt_sigma, alc_request.opt_nu, alc_request.amplitudes

    def _configure_action_chain(self):
        feature_block_action = AlCConstructFeatureBlock()
        self.action.set_next_action(feature_block_action)


class PickledDataAlaCarteFeatureConstructor(AlaCarteFeatureConstructor):

    def __init__(self, data_path, train_args, test_args, scale_y=True, model_path=None):
        self.logger = logging.getLogger('PickledDataAlaCarteFeatureConstructor')
        self._configure_next_action(model_path, train_args, test_args)
        self.action = LoadDataAndConfigureRequest(data_path, train_args, test_args, scale_y)
        self._configure_action_chain()
