############################################################################################################################################
# The MIT License (MIT)                                                                                                                    #
#                                                                                                                                          #
# Copyright (c) 2016 Dino Oglic & Thomas Gaertner                                                                                          #
#                                                                                                                                          #
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files         #
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,      #
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,   #
# subject to the following conditions:                                                                                                     #
#                                                                                                                                          #
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.           #
#                                                                                                                                          #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF       #
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR  #
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH   #
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                                               #
############################################################################################################################################


import numpy as np
import multiprocessing as mp
import logging

from scipy.optimize import fmin_l_bfgs_b
from sklearn.cross_validation import (StratifiedKFold, KFold)


def mp_fold_mse(gamma, tr_fold_args, vl_fold_args):
    y = np.frombuffer(lrm_mp_y).reshape(-1)
    X = np.frombuffer(lrm_mp_X).reshape(y.shape[0], -1)
    amplitudes = RidgeRegression.fit(X[tr_fold_args, :], y[tr_fold_args], gamma)
    residues = X[vl_fold_args, :].dot(amplitudes) - y[vl_fold_args]
    return np.mean(residues ** 2)


def mp_fold_mse_gradient(gamma, tr_fold_args, vl_fold_args):
    y = np.frombuffer(lrm_mp_y).reshape(-1)
    X = np.frombuffer(lrm_mp_X).reshape(y.shape[0], -1)
    amplitudes, K = RidgeRegression.fit_with_penalty_mat(X[tr_fold_args, :], y[tr_fold_args], gamma)
    residues = X[vl_fold_args, :].dot(amplitudes) - y[vl_fold_args]
    t = np.linalg.solve(K, np.mean(np.multiply(residues.reshape(-1, 1), X[vl_fold_args, :]), axis=0))
    return -4 * gamma * t.dot(amplitudes)


def mp_pool_init(X, y):
    global lrm_mp_X, lrm_mp_y
    lrm_mp_X, lrm_mp_y = X, y


class RidgeRegression(object):

    logger = logging.getLogger('RidgeRegression')

    '''
        The method fits a linear ridge regression model for a specified value of the regularization parameter

        Parameters:

            X : numpy array
                data matrix with instances as rows
                shape=(m, d), where m is the number of instances and d is the dimension of the problem

            y : numpy array
                the array with a target value per instance
                shape=(m,), where m is the number of instances

            gamma : float
                    regularization parameter

        Outputs:

            amplitudes : numpy array
                         coefficients of linear model
                         shape=(m,), where m is the number of instances
    '''
    @staticmethod
    def fit(X, y, gamma):
        return RidgeRegression.fit_with_penalty_mat(X, y, gamma)[0]

    '''
        The method fits a linear ridge regression model with a specified value of the regularization parameter and a penalty matrix

        Parameters:

            X : numpy array
                data matrix with instances as rows
                shape=(m, d), where m is the number of instances and d is the dimension of the problem

            y : numpy array
                the array with a target value per instance
                shape=(m,), where m is the number of instances

            gamma : float
                    regularization parameter

            penalty_mat : [Optional] numpy array
                          matrix to be used in place of the identity matrix as a multiple of the regularization parameter
                          shape=(d, d), where d is the dimension of the problem

        Outputs:

            amplitudes : numpy array
                         coefficients of linear model
                         shape=(m,), where m is the number of instances

            K : numpy array
                matrix that defines a linear system corresponding to a ridge regression problem, i.e., K = (1/m) X.T * X
                shape=(d, d), where d is the dimension of the problem
    '''
    @staticmethod
    def fit_with_penalty_mat(X, y, gamma, penalty_mat=None):
        K = X.T.dot(X) / X.shape[0]
        if penalty_mat is None:
            K.flat[::K.shape[0] + 1] += gamma ** 2 + 1e-10
        else:
            K += penalty_mat * gamma ** 2
            K.flat[::K.shape[0] + 1] += 1e-10
        amplitudes = np.linalg.solve(K, X.T.dot(y) / X.shape[0])
        return amplitudes, K

    """
        Linear ridge regression as a hyper-parameter optimization

        !!! This is a multiprocessing call and it is executed on min(num_cv_folds, cpu_count - 1) cores of a machine !!!

        Parameters:

            X : multiprocessing RawArray
                vector-buffer with concatenated training examples (instance by instance)

            y : multiprocessing RawArray
                vector-buffer with target values for training examples

            cv_args : sklearn KFold
                      indices of a k-fold data splitting

            init_sol : numpy array
                       initial solution of the hyper-parameter optimization problem
                       shape=(1,)

        Outputs:

            opt_reg_param : float
                            optimal regularization parameter

            vl_mse : float
                     the average mean squared error over validation samples in a k-fold cross-validation of the linear ridge regression
                     model specified with the optimal regularization parameter (@output: opt_reg_param)
    """
    @staticmethod
    def kfold_fit(X, y, cv_args, init_sol=np.array([1e-2])):
        fmin_output = fmin_l_bfgs_b(lambda z: RidgeRegression.mse(z, X, y, cv_args), init_sol,
                                    lambda z: RidgeRegression.mse_gradient(z, X, y, cv_args), disp=False, iprint=-1)
        num_cv_folds, opt_reg_param = len(cv_args), abs(fmin_output[0])
        RidgeRegression.logger.debug(str(num_cv_folds) + '-fold linear ridge regression')
        RidgeRegression.logger.info('--> optimal lambda:' + str(opt_reg_param))
        RidgeRegression.logger.info('--> validation error: ' + str(np.sqrt(fmin_output[1])))

        return opt_reg_param, fmin_output[1]

    '''
        The method computes the average mean squared error over validation samples in a k-fold cross-validation of the linear ridge
        regression model specified with a regularization parameter

        !!! This is a multiprocessing call and it is executed on min(num_cv_folds, cpu_count - 1) cores of a machine !!!

        Parameters:

            gamma : float
                    regularization parameter

            X : multiprocessing RawArray
                vector-buffer with concatenated training examples (instance by instance)

            y : multiprocessing RawArray
                vector-buffer with target values for training examples

            cv_args : sklearn KFold
                      indices of a k-fold data splitting

        Outputs:

            mse : float
                  the average mean squared error over validation samples in a k-fold cross-validation of the linear ridge regression model
                  specified with a regularization parameter (@parameters: gamma)
    '''
    @staticmethod
    def mse(gamma, X, y, cv_args):
        async_results, val, num_cv_folds = [], 0., len(cv_args)
        processor_pool = mp.Pool(min(num_cv_folds, mp.cpu_count() - 1), initializer=mp_pool_init, initargs=(X, y))
        for tr_fold_args, vl_fold_args in cv_args:
            async_results.append(processor_pool.apply_async(mp_fold_mse, (gamma, tr_fold_args, vl_fold_args)))
        processor_pool.close()
        processor_pool.join()

        for i in range(num_cv_folds):
            val += async_results[i].get()
        del async_results

        return val / num_cv_folds

    '''
        The method computes the gradient of the average mean squared error computed over validation samples in a k-fold cross-validation of
        the linear ridge regression model specified with a regularization parameter

        !!! This is a multiprocessing call and it is executed on min(num_cv_folds, cpu_count - 1) cores of a machine !!!

        Parameters:

            gamma : float
                    regularization parameter

            X : multiprocessing RawArray
                vector-buffer with concatenated training examples (instance by instance)

            y : multiprocessing RawArray
                vector-buffer with target values for training examples

            cv_args : sklearn KFold
                      indices of a k-fold data splitting

        Outputs:

            mse_gradient : numpy array
                           the gradient at the provided regularization parameter (@parameters: gamma) of the average mean squared error
                           objective function (@method: RidgeRegression.mse)
                           shape=(1,)
    '''
    @staticmethod
    def mse_gradient(gamma, X, y, cv_args):
        async_results, gradient, num_cv_folds = [], np.zeros(1), len(cv_args)
        processor_pool = mp.Pool(min(num_cv_folds, mp.cpu_count() - 1), initializer=mp_pool_init, initargs=(X, y))
        for tr_fold_args, vl_fold_args in cv_args:
            async_results.append(processor_pool.apply_async(mp_fold_mse_gradient, (gamma, tr_fold_args, vl_fold_args)))
        processor_pool.close()
        processor_pool.join()

        for i in range(num_cv_folds):
            gradient[0] += async_results[i].get()
        del async_results

        return gradient / num_cv_folds


class UnsupervisedKFold(object):

    @staticmethod
    def stratified_kfolds(stratified_labels, num_cv_folds):
        return StratifiedKFold(stratified_labels, num_cv_folds, shuffle=True)

    @staticmethod
    def kfolds(n, num_cv_folds):
        return KFold(n, num_cv_folds, shuffle=True)
