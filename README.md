### PUBLICATION ###
Greedy Feature Construction @ Advances in Neural Information Processing Systems 29 (NIPS), Barcelona 2016

### URL ###
http://papers.nips.cc/paper/6557-greedy-feature-construction.pdf

### ADMIN ###
dino.oglic [at] domain

domain = uni-bonn.de
